<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

function local_sharedspaceh_format_number_to_flip_display($number) {
    $number = (int)$number;
    $display = '';
    do {
        $digit = $number % 10;
        $display = '<span class="border border-dark p-1">' . $digit . '</span>' . $display;
        $number = (int)floor($number / 10);
    } while ($number > 0);
    $display = '<b style="font-family:var(--font-family-monospace)">' . $display . '</b>';
    return $display;
}

function local_sharedspaceh_welcome_message() {
    global $OUTPUT, $DB;

    $message = html_writer::tag('em', get_string('caseine_in_progress_message', 'local_sharedspaceh'));

    $sql = "SELECT COUNT(cm.id) as nshared, COUNT(DISTINCT c.id) as ncourses
            FROM {course_modules} cm
            JOIN {course} c ON c.id = cm.course
            JOIN {local_metadata} lm ON lm.instanceid = cm.id
            WHERE lm.data = 1 AND lm.fieldid = :sharedfieldid";
    $data = $DB->get_record_sql($sql, array('contextlevel' => CONTEXT_MODULE, 'sharedfieldid' => (int) get_config('local_sharedspaceh', 'metadatasharedfield')));

    $sharedicon = html_writer::img($OUTPUT->image_url('sharing', 'local_sharedspaceh'), ' ');
    $sharedicon = $OUTPUT->pix_icon('sharing', '', 'local_sharedspaceh', array('style' => 'width:20px;height:20px;vertical-align:text-bottom;'));
    $message .= html_writer::div($sharedicon .
            get_string('sharedmodulescount', 'local_sharedspaceh',
                    array_map('local_sharedspaceh_format_number_to_flip_display', (array)$data)
                    ), 'text-center my-3 ml-3');

    return $message;
}

/**
 *
 * @param context $context
 */
function local_sharedspaceh_assign_temporary_backuper_role($context) {
    global $USER;
    if (!isset($USER->access)) {
        load_all_capabilities();
    }

    $context->reload_if_dirty();

    $roleid = (int) get_config('local_sharedspaceh', 'backuperrole');

    if ($roleid && !isset($USER->access['ra'][$context->path][$roleid])) {
        $USER->access['ra'][$context->path][(int)$roleid] = (int)$roleid;
    }
}

function sharedspaceh_debug($data) {
    echo nl2br(str_replace(' ', '&nbsp;', json_encode($data, JSON_PRETTY_PRINT))) . '<br>';
}
