<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(__DIR__ . '/../../config.php');

global $CFG, $PAGE, $OUTPUT, $DB;

require_once(__DIR__ . '/spacelib.php');

use local_sharedspaceh\metadata\import\space_config_form,
    local_sharedspaceh\metadata\import\space_confirm_form,
    local_sharedspaceh\space_table;

require_login();

$context = context_system::instance();
$PAGE->set_context($context);
$url = new moodle_url('/local/sharedspaceh/configpage.php');
$PAGE->set_url($url);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('sharedspaceh', 'local_sharedspaceh'));

require_capability('local/sharedspaceh:accesstospaceh', $context, null, true, 'error_when_accessing_adminpage', 'local_sharedspaceh');

$mform = new space_config_form();

$confirmform = new space_confirm_form(null, array('changes' => '[]'));
if ($confirmform->is_cancelled()) {
    redirect($url);
    die();
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('metadatabulkedition', 'local_sharedspaceh'));
echo $OUTPUT->box_start();

if ($confirmeddata = $confirmform->get_data()) {

    confirm_sesskey();

    $formattedchanges = json_decode($confirmeddata->changes);

    // Everything is fine, save the submitted metadata.
    require_once($CFG->dirroot . '/local/metadata/lib.php');
    foreach ($formattedchanges as $cmid => $metadata) {
        $metadata = (object) $metadata;
        $metadata->id = $cmid;
        if (has_capability('moodle/course:manageactivities', context_module::instance($cmid))) {
            try {
                local_metadata_save_data($metadata, CONTEXT_MODULE);
            } catch (dml_exception $e) {
                // Exceptions can happen if data is invalid.
                $message = get_string('modulemetadatanotsaved', 'local_sharedspaceh',
                        array('cmid' => $cmid, 'details' => $e->getMessage()));
                echo $OUTPUT->notification($message, \core\output\notification::NOTIFY_ERROR);
            }
        }
    }

    echo $OUTPUT->notification(get_string('success'), \core\output\notification::NOTIFY_SUCCESS);
    echo $OUTPUT->continue_button($url);

} else if ($formdata = $mform->get_data()) {

    $file = $mform->save_temp_file('csv_auto_tag_file');
    if ($file === false) {
        // Not supposed to happen if form validation has been done correctly.
        throw new moodle_exception('invalidformdata', '', $url);
    }

    if (method_exists('\Box\Spout\Reader\CSV\Reader', 'setGlobalFunctionsHelper')) {
        $r = new \Box\Spout\Reader\CSV\Reader();
        $r->setGlobalFunctionsHelper(new \Box\Spout\Common\Helper\GlobalFunctionsHelper());
    } else {
        // Moodle 3.8+.
        $helperfactory = new \Box\Spout\Common\Creator\HelperFactory();
        $optionsmanager = new \Box\Spout\Reader\CSV\Manager\OptionsManager();
        $functionshelper = $helperfactory->createGlobalFunctionsHelper();
        $entityfactory = new \Box\Spout\Reader\CSV\Creator\InternalEntityFactory($helperfactory);
        $r = new \Box\Spout\Reader\CSV\Reader($optionsmanager, $functionshelper, $entityfactory);
    }
    $r->setFieldDelimiter($formdata->csvseparator);
    $r->setFieldEnclosure($formdata->stringseparator);
    $r->setEncoding($formdata->fileencoding);
    $r->open($file);
    $sheets = $r->getSheetIterator();
    $sheets->rewind();
    $rows = $sheets->current()->getRowIterator();

    try {
        // Skip BOM and empty lines at the beginning.
        $rows->rewind();
        while ($rows->valid() && empty($rows->current())) {
            $rows->next();
        }

        if (!$rows->valid()) {
            throw new moodle_exception('fileisempty', 'local_sharedspaceh');
        }

        // First row is the headers.
        $headers = $rows->current();
        if (is_object($headers) && get_class($headers) === 'Box\Spout\Common\Entity\Row') {
            $headers = $headers->toArray();
        }
        $columns = array_flip($headers);
        if (!isset($columns[space_table::CMID_COLUMN])) {
            throw new moodle_exception('missingcmidcolumn', 'local_sharedspaceh');
        }

        $table = new html_table();
        $table->head = array(
                get_string('cm_id', 'local_sharedspaceh'),
                get_string('activitymodule'),
                get_string('course')
        );
        $dataoutput = new \metadatacontext_module\output\manage_data();
        foreach ($dataoutput->data as $items) {
            unset($items['categoryname']);

            foreach ($items as $item) {
                if (!$item->field->visible) {
                    continue;
                }
                if (isset($columns[space_table::METADATA_COLUMN_PREFIX . $item->field->shortname])) {
                    $table->head[] = format_string($item->field->name);
                }
            }
        }

        $mdcolumnhaschanges = array();

        $nchanges = 0;
        $formattedchanges = array();

        $displayencodingwarning = false;
        $displaynullwarning = false;

        $rows->next();
        while ($rows->valid()) {
            $row = $rows->current();
            if (is_object($row) && get_class($row) === 'Box\Spout\Common\Entity\Row') {
                $row = $row->toArray();
            }
            $cmid = $row[$columns[space_table::CMID_COLUMN]];
            list($course, $cm) = get_course_and_cm_from_cmid($cmid, '', 0, -1);

            $icon = $OUTPUT->image_icon('icon', $cm->get_module_type_name(), $cm->modname, array('class' => 'activityicon'));
            $tablerow = array($cmid, '<span class="activity">' . $icon . $cm->get_formatted_name() . '</span>', format_string($course->fullname));

            $cmhaschanges = false;
            $cmformattedchanges = array();

            $cmdataoutput = new \metadatacontext_module\output\manage_data($cm);
            foreach ($cmdataoutput->data as $items) {
                unset($items['categoryname']);

                foreach ($items as $item) {
                    if (!$item->field->visible) {
                        continue;
                    }
                    if (isset($columns[space_table::METADATA_COLUMN_PREFIX . $item->field->shortname])) {
                        $oldvalue = $DB->get_field('local_metadata', 'data', array('fieldid' => $item->field->id, 'instanceid' => $cmid));
                        $oldvaluedisplay = $item->display_data();
                        $newvalue = $row[$columns[space_table::METADATA_COLUMN_PREFIX . $item->field->shortname]];
                        if (!mb_check_encoding($newvalue, $formdata->fileencoding)) {
                            // Sometimes accentuated characters are not well encoded - try to fix this.
                            $newvalue = mb_convert_encoding($newvalue, $formdata->fileencoding,
                                    $formdata->fileencoding == 'ISO-8859-1' ? 'UTF-8' : 'ISO-8859-1');
                            // Display a notice that some conversion has been done.
                            $displayencodingwarning = true;
                        }
                        $item->data = $item->edit_save_data_preprocess(is_object($newvalue) ? (array) $newvalue : $newvalue, new \stdClass());
                        $newvaluedisplay = $item->display_data();
                        if ($item->data === null) {
                            $displaynullwarning = true;
                            $newvaluedisplay = '<span>[<code>null</code>]</span>';
                        }

                        $info = '';
                        if ($oldvalue === false || $oldvalue === null) {
                            $info = '<b class="text-success mr-1">[' . get_string('new') . ']</b>';
                            $oldvaluedisplay = null;
                            $cmhaschanges = true;
                            $mdcolumnhaschanges[$item->field->shortname] = true;
                            $nchanges ++;
                            $cmformattedchanges[$item->inputname] = $newvalue;
                        } else if ($oldvaluedisplay == $newvaluedisplay) {
                            $info = '<b class="text-info mr-1">[' . get_string('unchanged', 'metadatacontext_module') . ']</b>';
                            $oldvaluedisplay = null;
                        } else {
                            $info = '<b class="text-warning mr-1">[' . get_string('overwrite', 'metadatacontext_module') . ']</b>';
                            $cmhaschanges = true;
                            $mdcolumnhaschanges[$item->field->shortname] = true;
                            $nchanges ++;
                            $cmformattedchanges[$item->inputname] = $newvalue;
                        }
                        if ($oldvaluedisplay === '') {
                            $oldvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                        }
                        if ($newvaluedisplay === '') {
                            $newvaluedisplay = '<span>[' . get_string('emptysettingvalue', 'admin') . ']</span>';
                        }
                        $tablerow[] = $info . ($oldvaluedisplay !== null ? $oldvaluedisplay . '<i class="fa fa-fw fa-caret-right"></i>' : '') . $newvaluedisplay;
                    }
                }
            }
            if ($cmhaschanges) {
                $table->data[] = $tablerow;
                $formattedchanges[$cmid] = $cmformattedchanges;
            }

            $rows->next();
        }

    } finally {
        // Close readers and delete files even in case of an error.
        $rows->end();
        $sheets->end();
        $r->close();
        unlink($file);
    }

    $i = 3;
    foreach ($dataoutput->data as $items) {
        unset($items['categoryname']);

        foreach ($items as $item) {
            if (isset($columns[space_table::METADATA_COLUMN_PREFIX . $item->field->shortname])) {
                if (empty($mdcolumnhaschanges[$item->field->shortname])) {
                    unset($table->head[$i]);
                    foreach ($table->data as &$tablerow) {
                        unset($tablerow[$i]);
                    }
                }
                $i++;
            }
        }
    }

    if ($nchanges > 0) {
        echo html_writer::tag('p', get_string('reviewchanges', 'local_sharedspaceh'));
        echo html_writer::div('<i class="fa fa-info-circle text-info mr-1"></i>' .
                get_string('detectedchanges', 'local_sharedspaceh',
                        array('changes' => $nchanges, 'modules' => count($table->data), 'fields' => count($table->head) - 3)
                ), 'my-2');
        if ($displayencodingwarning) {
            echo html_writer::div('<i class="fa fa-warning text-warning mr-1"></i>' .
                    get_string('encodingconversionwarning', 'local_sharedspaceh', $formdata->fileencoding),
                    'my-2');
        }
        if ($displaynullwarning) {
            echo html_writer::div('<i class="fa fa-warning text-warning mr-1"></i>' .
                    get_string('nullconversionwarning', 'local_sharedspaceh'),
                    'my-2');
        }
        echo html_writer::table($table);
        $confirmform->set_data(array('changes' => json_encode($formattedchanges)));
        $confirmform->display();
    } else {
        echo html_writer::div(get_string('nochangedetected', 'local_sharedspaceh'), 'text-center mb-3');
        echo $OUTPUT->continue_button($url);
    }

} else {
    $mform->display();
}

echo $OUTPUT->box_end();
echo $OUTPUT->footer();
