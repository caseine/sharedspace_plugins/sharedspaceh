// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define(['jquery', 'core/config', 'core/url', 'core/notification'], function ($, cfg, url, notification) {

    const ajaxFile = '/local/sharedspaceh/ajax/update_space_columns_preferences.php';

    function updateColumnPreference(colid, state) {
        var data = {
            sesskey: cfg.sesskey,
            column: colid,
            action: state
        };
        $.ajax({
            url: url.relativeUrl(ajaxFile),
            method: 'POST',
            data: data,
        })
        .done(function(response) {
            if (!response.success) {
                throw new Error(response.error);
            }
        })
        .fail(function(jqxhr, errorcode, message) {
             notification.exception({
                 errorcode: errorcode,
                 message: message
             });
        });
    }

    return {
        init: function(columnsNames) {
            var $columnsMenu = $('[role="table-columns-menu"]');
            $('table th.header').each(function(i) {
                var $menuOption = $columnsMenu.find('[data-colnum="' + i + '"]');
                if (!$(this).is('.nothideable')) {
                    var hideColumnStr = M.util.get_string('hidecolumn', 'local_sharedspaceh');
                    var $hideicon = $('<i class="fa fa-minus clickable" title="' + hideColumnStr + '"></i>')
                    .click(function() {
                        $('table .c' + i).addClass('hidden');
                        $menuOption.show();
                        updateColumnPreference(columnsNames[i], 'hide');
                    });
                    $(this).append($('<div>').append($hideicon));

                    if (!$(this).is('.hidden')) {
                        $menuOption.hide();
                    }
                } else {
                    $menuOption.remove();
                }
            });

            $columnsMenu.children().click(function() {
                $('table .c' + $(this).data('colnum')).removeClass('hidden');
                $(this).hide();
                updateColumnPreference($(this).data('colid'), 'show');
            });

            $('[role="add-table-columns"]').click(function() {
                $columnsMenu.toggle();
            }).show();

            document.onclick = function(e) {
                if (!$(e.target).closest('[role="table-columns-menu"], [role="add-table-columns"]').length) {
                    // This is a click outside of the columns menu, hide it.
                    $columnsMenu.hide();
                }
            };
        }
    };

});