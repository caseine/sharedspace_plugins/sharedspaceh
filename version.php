<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package local_sharedspaceh
 * @author Hadrien Cambazard, Nicolas Catusse
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017 onwards Les zozos de chez caseine
 */
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2022121401;
$plugin->requires  = 2016052300;
$plugin->component = 'local_sharedspaceh';
$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = 'Zarbacam Zarbison 2.0';

$plugin->dependencies = array('local_metadata' => ANY_VERSION, 'block_sharing_cart' => ANY_VERSION);
