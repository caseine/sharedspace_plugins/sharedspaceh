<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Extends the navigation bar when the plugin is active
 */
function local_sharedspaceh_extend_navigation(global_navigation $nav) {

    $spacemenu = $nav->add(get_string('sharedspaceh', 'local_sharedspaceh'), new moodle_url('/local/sharedspaceh/index.php'),
            navigation_node::TYPE_CUSTOM, null, null, new pix_icon('main_menu', ' ', 'local_sharedspaceh'));

    $tagging = $spacemenu->add_node(new local_sharedspaceh\navigation\space_navigation_node(
            array(
                    'text' => get_string('tagging_menu_name', 'local_sharedspaceh'),
                    'action' => new moodle_url('/local/sharedspaceh/configpage.php'),
                    'type' => navigation_node::TYPE_CUSTOM,
                    'key' => 'local_sharedspaceh_tagging',
                    'icon' => new pix_icon('tagging_menu', ' ', 'local_sharedspaceh')
            )));
    if (has_capability('local/sharedspaceh:accessasconfig', context_system::instance())) {
        $config = $spacemenu->add_node(new local_sharedspaceh\navigation\space_navigation_node(
                array(
                        'text' => get_string('config_menu_name', 'local_sharedspaceh'),
                        'action' => new moodle_url('/local/metadata/index.php', array('contextlevel' => CONTEXT_MODULE)),
                        'type' => navigation_node::TYPE_CUSTOM,
                        'key' => 'local_sharedspaceh_config',
                        'icon' => new pix_icon('config_menu', ' ', 'local_sharedspaceh')
                )));
    }

    // Shared space menu entry is always visible.
    $spacemenu->showinflatnavigation = true;
    // It has subentries.
    $spacemenu->isexpandable = true;
    // Show these menus if we are on one of the three pages.
    $tagging->showinflatnavigation = $spacemenu->contains_active_node();
    if (isset($config)) {
        $config->showinflatnavigation = $spacemenu->contains_active_node();
    }

}

/**
 * Get icon mapping for font-awesome.
 */
function local_sharedspaceh_get_fontawesome_icon_map() {
    return [
        'local_sharedspaceh:main_menu' => 'fa-space-shuttle',
        'local_sharedspaceh:tagging_menu' => 'fa-rocket',
        'local_sharedspaceh:config_menu' => 'fa-star',
    ];
}
