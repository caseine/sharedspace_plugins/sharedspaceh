<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Sharing Cart
 *
 * @package    local_sharedspaceh
 * @copyright  2017 (C) VERSION2, INC.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Sharing Cart upgrade
 *
 * @global moodle_database $DB
 */
function xmldb_local_sharedspaceh_upgrade($oldversion = 0) {
    global $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2018060700) { //HADRIEN: add the field to store the old id
        $table1 = new xmldb_table('local_sharedspaceh_teams');
        if (!$dbman->table_exists($table1)) {
            $table1->add_field('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table1->add_field('teamname', XMLDB_TYPE_TEXT, 32, null, XMLDB_NOTNULL, null, null);
            $table1->add_field('capabilityid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table1->add_key('id', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table1);
        }

        $table3 = new xmldb_table('local_sharedspaceh_teamfld');
        if (!$dbman->table_exists($table3)) {
            $table3->add_field('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table3->add_field('fieldid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table3->add_field('teamid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table3->add_key('id', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table3);
        }
    }

    if ($oldversion < 2018061200) { //NICO: add the automatic field table
        $table4 = new xmldb_table('local_sharedspaceh_autofld');
        if (!$dbman->table_exists($table4)) {
            $table4->add_field('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table4->add_field('fieldid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table4->add_field('request', XMLDB_TYPE_TEXT, null, null, null, null, null);
            $table4->add_key('id', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table4);
        }
    }

    if ($oldversion < 2019040001) { //Flo: add table for the block research configuration
        $table5 = new xmldb_table('local_sharedspaceh_fld_usrcf');
        if (!$dbman->table_exists($table5)) {
            $table5->add_field('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table5->add_field('id_user', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table5->add_field('id_field', XMLDB_TYPE_TEXT, 80, null, XMLDB_NOTNULL, null, null);
            $table5->add_field('visible', XMLDB_TYPE_INTEGER, 1, null, XMLDB_NOTNULL, null, 1);
            $table5->add_key('id', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table5);
        }
    }

    if ($oldversion < 2019120001) {
        $table6 = new xmldb_table('local_sharedspaceh_clm_usrcf');
        if (!$dbman->table_exists($table6)) {
            $table6->add_field('id', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table6->add_field('id_user', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, null);
            $table6->add_field('id_column', XMLDB_TYPE_TEXT, 80, null, XMLDB_NOTNULL, null, null);
            $table6->add_field('visible', XMLDB_TYPE_INTEGER, 1, null, XMLDB_NOTNULL, null, 1);
            $table6->add_key('id', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table6);
        }
    }

    if ($oldversion < 2022093001) { // Version Zarbacam Zarbison 2.0.

        // Drop all the tables.

        // Define table local_sharedspaceh_autofld to be dropped.
        $table = new xmldb_table('local_sharedspaceh_autofld');

        // Conditionally launch drop table for local_sharedspaceh_autofld.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table local_sharedspaceh_clm_usrcf to be dropped.
        $table = new xmldb_table('local_sharedspaceh_clm_usrcf');

        // Conditionally launch drop table for local_sharedspaceh_clm_usrcf.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table local_sharedspaceh_fld_usrcf to be dropped.
        $table = new xmldb_table('local_sharedspaceh_fld_usrcf');

        // Conditionally launch drop table for local_sharedspaceh_fld_usrcf.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table local_sharedspaceh_teamfld to be dropped.
        $table = new xmldb_table('local_sharedspaceh_teamfld');

        // Conditionally launch drop table for local_sharedspaceh_teamfld.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Define table local_sharedspaceh_teams to be dropped.
        $table = new xmldb_table('local_sharedspaceh_teams');

        // Conditionally launch drop table for local_sharedspaceh_teams.
        if ($dbman->table_exists($table)) {
            $dbman->drop_table($table);
        }

        // Sharedspaceh savepoint reached.
        upgrade_plugin_savepoint(true, 2022093001, 'local', 'sharedspaceh');
    }

    if ($oldversion < 2022121400) {

        // Define table local_sharedspaceh_suggest to be created.
        $table = new xmldb_table('local_sharedspaceh_suggest');

        // Adding fields to table local_sharedspaceh_suggest.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('fieldname', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('suggestions', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table local_sharedspaceh_suggest.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Adding indexes to table local_sharedspaceh_suggest.
        $table->add_index('fieldname', XMLDB_INDEX_UNIQUE, ['fieldname']);

        // Conditionally launch create table for local_sharedspaceh_suggest.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Sharedspaceh savepoint reached.
        upgrade_plugin_savepoint(true, 2022121400, 'local', 'sharedspaceh');
    }

    return true;
}
