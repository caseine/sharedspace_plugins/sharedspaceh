<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    $settings = new admin_settingpage('local_sharedspaceh', 'Caseine Shared Space');
    $settings->add(new admin_setting_configtext('local_sharedspaceh/option', 'Caseine Shared Space Option', 'Information about this option', 100, PARAM_INT));
    $settings->add(new admin_setting_configcheckbox('local_sharedspaceh/zozo', get_string('introzozo', 'local_sharedspaceh'), 'Aaaaah', 0));

    $metadataplugin = core_component::get_plugin_directory('local', 'metadata');
    if ($metadataplugin !== null) {
        global $DB;
        $fields = $DB->get_records_menu('local_metadata_field', array('contextlevel' => CONTEXT_MODULE), '', 'id, name');
        $fieldsmenu = array_map('format_string', $fields);
        $fieldornonemenu = array(0 => get_string('none')) + $fieldsmenu;
        $settings->add(
                new admin_setting_configselect(
                        'local_sharedspaceh/metadatasharedfield',
                        get_string('settings:metadatasharedfield', 'local_sharedspaceh'),
                        get_string('settings:metadatasharedfield_desc', 'local_sharedspaceh'),
                        0,
                        $fieldornonemenu
                        )
                );

        $settings->add(
                new admin_setting_configselect(
                        'local_sharedspaceh/metadataoriginalcmidfield',
                        get_string('settings:metadataoriginalcmidfield', 'local_sharedspaceh'),
                        get_string('settings:metadataoriginalcmidfield_desc', 'local_sharedspaceh'),
                        0,
                        $fieldornonemenu
                        )
                );

        if (!empty($fieldsmenu)) {
            $settings->add(
                    new admin_setting_configmultiselect(
                            'local_sharedspaceh/metadatafeaturedfields',
                            get_string('settings:metadatafeaturedfields', 'local_sharedspaceh'),
                            get_string('settings:metadatafeaturedfields_desc', 'local_sharedspaceh'),
                            array(),
                            $fieldsmenu
                            )
                    );
        }
    }

    $roles = role_get_names(context_system::instance(), ROLENAME_ALIAS, true);
    $settings->add(
            new admin_setting_configselect(
                    'local_sharedspaceh/backuperrole',
                    get_string('settings:backuperrole', 'local_sharedspaceh'),
                    get_string('settings:backuperrole_desc', 'local_sharedspaceh'),
                    0,
                    array(0 => get_string('none')) + $roles
                    )
            );

    $ADMIN->add('localplugins', $settings);

    $settings = null;
}
