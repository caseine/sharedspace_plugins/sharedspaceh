<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package shared_space
 * @author Hadrien Cambazard, Nicolas Catusse
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017 onwards Les zozos de chez caseine
 */

$string['pluginname'] = 'Shared Caseine Space (h)';
$string['sharedspaceh'] = 'Caseine Shared Space';

$string['cm_id'] = 'ID';
$string['cm_id_form'] = 'CM ID';
$string['cm_id_form_help'] = 'Id of the module in moodle\'s database';
$string['cm_name'] = 'Module';
$string['cm_name_form'] = 'Module name';
$string['cm_name_form_help'] = 'Name of the module in moodle\'s database';
$string['course_name'] = 'Course';
$string['course_name_form'] = 'Course name';
$string['course_name_form_help'] = 'Name of the course in moodle\'s database';
$string['cm_type'] = 'Type';
$string['cm_type_form'] = 'Module type';
$string['cm_type_form_help'] = 'Type of the module in moodle\'s database';
$string['mod_intro_form'] = 'Module intro';
$string['mod_intro_form_help'] = 'Keywords to search within the module\'s description';
$string['timecreated'] = 'Created';
$string['timecreated_form'] = 'Creation date';
$string['timecreated_form_help'] = 'Filter on the time the module was created.';
$string['timemodified'] = 'Last modified';
$string['timemodified_form'] = 'Last modification date';
$string['timemodified_form_help'] = 'Filter on the time the module was last modified.<br>
Please note that some modules may not implement this information, and would be filtered out if this filter is set to any value different than "Not specified".';

$string['addtocart'] = 'Add to cart';

$string['columns'] = 'Columns';
$string['hidecolumn'] = 'Hide this column';

$string['settings:metadatasharedfield'] = 'Space metadata Shared field';
$string['settings:metadatasharedfield_desc'] = 'The module metadata field that will be used to decide if an activity is shared or not.';
$string['settings:metadataoriginalcmidfield'] = 'Space metadata Original CM ID field';
$string['settings:metadataoriginalcmidfield_desc'] = '(Optional) The module metadata field that will be used to retrieve which module is the original of another.';
$string['settings:metadatafeaturedfields'] = 'Space metadata featured fields';
$string['settings:metadatafeaturedfields_desc'] = '(Optional) The module metadata fields that will be displayed on top of the advanced search form.';
$string['settings:backuperrole'] = 'Backuper role';
$string['settings:backuperrole_desc'] = 'A role that will be temporarily assigned to users of the Shared Space for each shared module, in order to allow backup.<br>
Please select a role that can be assigned to Course Module level, and that allows the moodle/backup:backupactivity capability.';

$string['introzozo'] = 'Shared Caseine Space Zozos Allowed?';

$string['error_when_accessing'] = 'This space is dedicated to teachers. If you are a teacher then you are not yet part of the caseine community. Please contact the caseine team to start your class on the plateform and you will get access to all shared contents.';
$string['error_when_accessing_adminpage'] = 'This space is restricted to caseine administrators since it is still under developpement. It will be soon open to all caseine members.';

$string['sharedspaceh:accesstospaceh'] = 'Capability to access the shared space';
$string['sharedspaceh:accessasadmin'] = 'Capability to administrate the shared space';
$string['sharedspaceh:accessasconfig'] = 'Capability to access the module metadata fields configuration page';

$string['nocoursefound'] = 'No activity matching these search criteria.';
$string['filterby'] = 'Filter by {$a}';
$string['filterbymetadata'] = 'Filter by module metadata fields';
$string['filterbyadvancedmetadata'] = 'Filter by module metadata advanced fields';
$string['filterbymodulefields'] = 'Filter by module standard fields';
$string['searchoptions'] = 'Search options';
$string['include_modules_search'] = 'Include the modules I am allowed to edit';
$string['include_modules_search_help'] = 'If enabled, the search will include all the modules you can edit across the platform, in addition to shared ones.<br>
This is especially useful if you want to bulk edit modules metadata. <a href="configpage.php">More information</a>';
$string['search_all_the_plateform'] = 'Search all the plateform';
$string['searchcheckbox'] = 'Scope of search';
$string['searchcheckbox_help'] = 'If enabled, the search is performed over all the plateform. If disabled, the search is restricted to modules shared to the community of teachers (marked as shared)';
$string['notspecified'] = 'Not specified';
$string['match'] = 'Match';
$string['matchany'] = 'Match any';
$string['contain'] = 'Contain';
$string['all'] = 'all';
$string['any'] = 'any';
$string['of'] = 'of';
$string['before'] = 'Before';
$string['after'] = 'After';
$string['between'] = 'Between';
$string['exactdate'] = 'Exact date';

$string['caseine_in_progress_message'] = 'The caseine shared space is still under development. It is currently functional and making progress!';
$string['sharedmodulescount'] = 'Shared modules: {$a->nshared}, from {$a->ncourses} different courses!';

$string['search'] = 'Search';

$string['result'] = 'Results: {$a->nresults} in {$a->seconds}s';
$string['processingtime'] = 'Results processing took {$a->seconds}s.';

$string['update_suggestions_task'] = 'Update search suggestions for Shared Space';

$string['thisisdefault'] = 'This is the default data for this field';
$string['showallintro'] = 'Show all, formatted description';
$string['thisitemispopular'] = 'This item is popular! It has been copied {$a->n} times in {$a->c} different courses.';

$string['tagging_menu_name'] = 'Tagging meta-data';
$string['config_menu_name'] = 'Metadata Config';

$string['metadatabulkedition'] = 'Metadata bulk edition';
$string['downloadcsv'] = 'Download editable modules metadata';
$string['csvseparator'] = 'Values separator';
$string['csvseparator_semicolon'] = 'Semicolon';
$string['csvseparator_comma'] = 'Comma';
$string['csvseparator_tab'] = 'Tab';
$string['csvseparator_space'] = 'Space';
$string['stringseparator'] = 'String separator';
$string['stringseparator_doublequote'] = 'Double quote';
$string['stringseparator_simplequote'] = 'Simple quote';
$string['fileencoding'] = 'File encoding';
$string['fileisempty'] = 'Error: no metadata found in uploaded file.';
$string['missingcmidcolumn'] = 'Error: missing cm_id column in uploaded file.';
$string['reviewchanges'] = 'Please review the changes about to be applied then click "Confirm" at the bottom of the table.';
$string['detectedchanges'] = 'Detected {$a->changes} changes on {$a->modules} modules, for {$a->fields} metadata fields.';
$string['nochangedetected'] = 'No changes on metadata detected from submitted file.';
$string['encodingconversionwarning'] = 'Some values encodings were inconsistent with the specified file encoding ({$a}). These values were re-encoded. Please double check the values in the table below, especially accentued text.';
$string['nullconversionwarning'] = 'Some values were converted to <code>null</code> by preprocessing. This can be caused by bad character encoding. This may lead to errors when writing into the database. Please click "Cancel" if this was not intended.';
$string['modulemetadatanotsaved'] = 'Metadata not saved or partially saved for course module of ID {$a->cmid}:<br>
{$a->details}';
$string['user_guide'] = 'User guide';
$string['parsing_and_tagging_from_csv'] = 'Parsing and tagging from .csv';
$string['parse_and_load'] = 'Upload';
$string['file_choose_one'] = 'File (Choose one)';
$string['user_guide_content'] = '<p>How to tag your modules with a <strong>.csv</strong> file:</p>
<ul>
<li>First step: download the .csv file. This can be done from the <a href="index.php">Shared Space</a> as follows:
    <ul>
    <li>Tick the "Include the modules I am allowed to edit" check box in the Search options. This adds in the results the modules you are allowed to manage even if they are not shared.</li>
    <li>You can specify filters like a normal search in the shared space (such as a course name to only edit modules within one course).</li>
    <li>Click "Search"</li>
    <li>From the results page, download the .csv file by clicking "Download editable modules metadata" at the top of the table.</li>
    </ul>
</li>
<li>Second step: edit the .csv file.
    <ul>
    <li>Please <b>do not remove or edit</b> the column "cm_id", nor the headers line! These are used to index the data.</li>
    <li>You can safely remove any other line or column (e.g. modules or column you do not want to edit anything on).</li>
    <li>Please note that only modifications on metadata columns will be taken into account. Module type, name or course name are only included as information.</li>
    <li>Some metadata fields are constrained to a predefined set of values (pick a particular module and check the drop-down menus of the metadata). If you provide an invalid value, it will be converted to <code>null</code>.</li>
    <li>Boolean values (e.g. checkboxes) are set with 0/1 in the .csv.</li>
    </ul>
<li>Final step: upload the edited .csv file in the section below.</li>
    <ul>
    <li>Upload your .csv file with the corresponding settings (MS Excel, LibreOffice Calc, etc. may save it with different values separators).</li>
    <li>You will be able to review the changes before they are applied.</li>
    </ul>
</ul>';
