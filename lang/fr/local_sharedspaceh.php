<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package shared_space
 * @author Hadrien Cambazard, Nicolas Catusse
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright 2017 onwards Les zozos de chez caseine
 */

$string['sharedspaceh'] = 'Espace de partage';

$string['cm_id'] = 'ID';
$string['cm_id_form'] = 'CM ID';
$string['cm_id_form_help'] = 'ID du module dans la base de données moodle';
$string['cm_name'] = 'Module';
$string['cm_name_form'] = 'Nom du module';
$string['cm_name_form_help'] = 'Nom du module dans la base de données moodle';
$string['course_name'] = 'Cours';
$string['course_name_form'] = 'Nom du cours';
$string['course_name_form_help'] = 'Nom du cours dans la base de données moodle';
$string['cm_type'] = 'Type';
$string['cm_type_form'] = 'Type de module';
$string['cm_type_form_help'] = 'Type de module dans la base de données moodle';
$string['mod_intro_form'] = 'Intro du module';
$string['mod_intro_form_help'] = 'Mots-clés à chercher dans la description du module';
$string['timecreated'] = 'Création';
$string['timecreated_form'] = 'Date de création';
$string['timecreated_form_help'] = 'Filtre sur la date de création du module.';
$string['timemodified'] = 'Dernière modification';
$string['timemodified_form'] = 'Date de dernière modification';
$string['timemodified_form_help'] = 'Filtre sur la date de dernière modification du module.<br>
À noter que certains types de modules n\'implémentent pas cette information, et seront exclus par le filtre pour tout autre valeur que "Non spécifié".';

$string['addtocart'] = 'Ajouter au panier';

$string['columns'] = 'Colonnes';
$string['hidecolumn'] = 'Cacher cette colonne';

$string['error_when_accessing'] = 'Cet espace est dédié aux enseignants.<br>
Si vous êtes un enseignant et voyez ce message, alors vous ne faites pas (encore) partie de la communauté enseignante.<br>
Veuillez contacter l\'équipe Caseine pour commencer votre cours et avoir accès aux ressources partagées !';
$string['error_when_accessing_adminpage'] = 'Cette page fait partie d\'un espace réservé aux administrateurs.<br>
Nous travaillons à un moyen de la rendre accessible à la communauté.<br>
Un peu de patience !';

$string['nocoursefound'] = 'Aucun résultat trouvé correspondant aux critères de recherche.';
$string['filterby'] = 'Filtrer par {$a}';
$string['filterbymetadata'] = 'Filtrer par métadonnées des modules';
$string['filterbyadvancedmetadata'] = 'Filtrer par métadonnées avancées des modules';
$string['filterbymodulefields'] = 'Filtrer par champs standards des modules';
$string['searchoptions'] = 'Options de recherche';
$string['include_modules_search'] = 'Inclure les modules sur lesquels je possède des droits d\'édition';
$string['include_modules_search_help'] = 'Si cette case est cochée, les résultats de la recherche incluront les modules sur lesquels vous possédez des droits d\'édition, en plus des modules partagés.<br>
Cette option est utile si vous voulez faire de l\'édition de masse des métadonnées. <a href="configpage.php">Plus d\'informations</a>';
$string['search_all_the_plateform'] = 'Étendre la recherche à toute la plateforme';
$string['searchcheckbox'] = 'l\'étendue de la recherche';
$string['searchcheckbox_help'] = 'Si cette case est cochée, la recherche portera sur tous les modules de la plateforme.<br>
Sinon, la recherche sera limitée aux modules partagés avec la communauté enseignante.';
$string['notspecified'] = 'Non spécifié';
$string['match'] = 'Correspond à';
$string['matchany'] = 'Correspond à au moins un';
$string['contain'] = 'Contient';
$string['all'] = 'tous';
$string['any'] = 'au moins un';
$string['of'] = 'parmi';
$string['before'] = 'Avant';
$string['after'] = 'Après';
$string['between'] = 'Dans l\'intervalle';
$string['exactdate'] = 'Date exacte';

$string['caseine_in_progress_message'] = 'L\'espace de partage est toujours en cours de développement. Il est cependant fonctionnel et en progrès !';
$string['sharedmodulescount'] = 'Modules partagés : {$a->nshared}, depuis {$a->ncourses} cours différents !';

$string['search'] = 'Rechercher';

$string['result'] = 'Résulats : {$a->nresults} en {$a->seconds}s';
$string['processingtime'] = 'Le traitement des résultats a pris {$a->seconds}s.';

$string['thisisdefault'] = 'Valeur par défaut pour ce champ';
$string['showallintro'] = 'Afficher toute la description formatée';
$string['thisitemispopular'] = 'Cet item est populaire ! Il a été copié {$a->n} fois vers {$a->c} cours différents.';

$string['metadatabulkedition'] = 'Édition de masse des métadonnées';
$string['downloadcsv'] = 'Télécharger les métadonnées des modules éditables';
$string['csvseparator'] = 'Séparateur de valeurs';
$string['csvseparator_semicolon'] = 'Point-virgule';
$string['csvseparator_comma'] = 'Virgule';
$string['csvseparator_tab'] = 'Tabulation';
$string['csvseparator_space'] = 'Espace';
$string['stringseparator'] = 'Séparateur de chaînes de caractères';
$string['stringseparator_doublequote'] = 'Quote double';
$string['stringseparator_simplequote'] = 'Quote simple';
$string['fileencoding'] = 'Encodage du fichier';
$string['fileisempty'] = 'Erreur : aucune métadonnée trouvée dans le fichier fourni.';
$string['missingcmidcolumn'] = 'Erreur : la colonne cm_id est manquante dans le fichier fourni.';
$string['reviewchanges'] = 'Veuillez relire les changements qui seront appliqués, puis cliquer sur "Confirmer" en bas du récapitulatif.';
$string['detectedchanges'] = '{$a->changes} changements détectés sur {$a->modules} modules, pour {$a->fields} champs de métadonnées.';
$string['nochangedetected'] = 'Aucun changement détecté dans le fichier fourni.';
$string['encodingconversionwarning'] = 'L\'encodage de certaines valeurs était incohérent avec l\'encodage spécifié ({$a}). Ces valeurs ont été ré-encodées. Veuillez vérifier avec attention les valeurs dans le tableau ci-dessous, et tout particulièrement les textes avec caractères accentués.';
$string['nullconversionwarning'] = 'Certaines valeurs ont été converties en <code>null</code> par le pré-traitement. Une cause probable est un mauvais encodage des caractères. Cela peut provoquer erreurs lors de l\'écriture vers la base de données. Veuillez cliquer sur "Annuler" si cela n\'était pas prévu.';
$string['modulemetadatanotsaved'] = 'Métadonnées non sauvegardées ou partiellement sauvegardées pour le module d\'ID {$a->cmid} :<br>
{$a->details}';
$string['user_guide'] = 'Guide utilisateur';
$string['parsing_and_tagging_from_csv'] = 'Traitement des métadonnées via .csv';
$string['parse_and_load'] = 'Téléverser';
$string['file_choose_one'] = 'Fournissez un fichier';
$string['user_guide_content'] = '<p>Comment tagger vos modules (en masse) avec un fichier <strong>.csv</strong> :</p>
<ul>
<li>Première étape : téléchargez le fichier .csv. Vous pouvez le récupérer en passant par <a href="index.php">l\'Espace de partage</a> comme suit :
    <ul>
    <li>Cochez la case "Inclure les modules sur lesquels je possède des droits d\'édition" dans les Options de recherche. Cela ajoutera aux résultats les modules que vous pouvez éditer aux modules partagés.</li>
    <li>Vous pouvez spécifier des filtres comme pour une recherche classique sur l\'espace de partage (comme un nom de cours pour éditer seulement les modules d\'un cours).</li>
    <li>Cliquez sur "Rechercher"</li>
    <li>Sur la page des résultats, téléchargez le fichier .csv en cliquant sur "Télécharger les métadonnées des modules éditables" en haut du tableau.</li>
    </ul>
</li>
<li>Deuxième étape : éditez le fichier .csv.
    <ul>
    <li>Merci de <b>ne pas supprimer ou modifier</b> la colonne "cm_id", ni la ligne d\'en-têtes ! Elles sont utilisées pour indexer les données.</li>
    <li>Vous pouvez supprimer sans risque les autres lignes ou colonnes (commes les modules ou les colonnes pour lesquels vous ne voulez rien modifier).</li>
    <li>À noter que seules les modifications sur les métadonnées seront prises en compte. Le type de module, le nom du module et le nom du cours sont inclus uniquement à titre informatif.</li>
    <li>Certains champs de métadonnées sont restreints à un certain ensemble de valeurs pré-définies (éditez les métadonnées d\'un module et observez le menu déroulant du champ). Si vous fournissez une valeur invalide, elle sera convertie en <code>null</code>.</li>
    <li>Les valeurs booléennes (e.g. checkbox) sont paramétrées par 0/1 dans le .csv.</li>
    </ul>
<li>Dernière étape : téléversez le fichier .csv dans la section ci-dessous.</li>
    <ul>
    <li>Téléversez votre fichier .csv avec les paramètres adaptés (MS Excel, LibreOffice Calc, etc. peuvent sauvegarder le fichier avec des sépérateurs différents).</li>
    <li>Vous pourrez effectuer une relecture des changements avant que ceux-ci soient appliqués.</li>
    </ul>
</ul>';
