<?php
// This file is part of VPL for Moodle - http://vpl.dis.ulpgc.es/
//
// VPL for Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// VPL for Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with VPL for Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh\task;

defined('MOODLE_INTERNAL') || die();

use local_sharedspaceh\space_skeleton;

class update_suggestions_task extends \core\task\scheduled_task {

    /**
     * {@inheritDoc}
     * @see \core\task\scheduled_task::get_name()
     */
    public function get_name() {
        return get_string('update_suggestions_task', 'local_sharedspaceh');
    }

    /**
     * {@inheritDoc}
     * @see \core\task\task_base::execute()
     */
    public function execute() {
        global $DB;

        $sharedfieldid = (int) get_config('local_sharedspaceh', 'metadatasharedfield');

        // Module metadata fields.
        $fieldsuggestions = array();
        foreach (space_skeleton::getOrderedMetadataFields() as $field) {
            if (!$field->visible || $field->id == $sharedfieldid || in_array($field->datatype, array('checkbox', 'menu', 'datetime'))) {
                continue;
            }

            $metadata = $DB->get_fieldset_sql(
                    "SELECT lm1.data
                      FROM {local_metadata} lm1
                      JOIN {local_metadata} lm2 ON lm2.instanceid = lm1.instanceid
                     WHERE lm1.fieldid = " . $field->id . " AND lm2.fieldid = ? AND lm2.data = 1
                  GROUP BY lm1.instanceid",
                    array($sharedfieldid));

            $fieldsuggestions[space_skeleton::METADATA_FIELD_PREFIX . $field->shortname] = $this->clean_suggestions($metadata);
        }

        // Module standard fields.
        foreach (array_keys(space_skeleton::STANDARD_MODULE_FIELDS) as $field) {
            $method = 'compute_' . $field . '_suggestions';
            if (method_exists($this, $method)) {
                $fieldsuggestions[$field] = $this->$method();
            }
        }

        // Update the database.
        foreach ($fieldsuggestions as $fieldname => $suggestions) {
            $this->update_record($fieldname, $suggestions);
        }
    }

    protected function clean_suggestions($suggestions, $textsplit = false) {
        $cleanedsuggestions = array();
        foreach ($suggestions as $value) {
            // Split suggestions along html tags, commas, quotes and carriage returns,
            // as full matches on these values would not work properly anyway.
            if ($textsplit) {
                // We are splitting full text, so split it along sentences as well.
                // Only allow words, spaces, HTML non-breakable spaces, and things like "John's" or "semi-truck".
                $splitregex = "/<[^>]+>|[^\\d\\w &;'-]|(?<!\\w)('|-)|('|-)(?!\\w)|&(?!nbsp;)|(?<!&nbsp);/u";
            } else {
                $splitregex = '/<[^>]+>|,|"|\\r|\\n/';
            }

            $splitted = preg_split($splitregex, $value, null, PREG_SPLIT_NO_EMPTY);

            if ($textsplit) {
                // Trim suggestions of HTML non-breakable spaces.
                $splitted = array_map(function($value) {
                    // Replace &nbsp; by a character we already removed in preg_split, trim, and revert the replace.
                    return str_replace('#', '&nbsp;', trim(str_replace('&nbsp;', '#', $value), '# '));
                }, $splitted);
            }
            $cleanedsuggestions = array_merge($cleanedsuggestions, $splitted);
        }
        $cleanedsuggestions = $this->array_remove_duplicates(array_filter(array_map('trim', $cleanedsuggestions), 'strlen'));
        sort($cleanedsuggestions, SORT_NATURAL | SORT_FLAG_CASE);
        return $cleanedsuggestions;
    }

    /**
     * Remove duplicate values from an array, keeping best value for values that are equal in a case-insensitive way.
     * @param array $array
     * @return array
     */
    protected function array_remove_duplicates($array) {
        // Formats, by order of preference : Abcde, abcde, ABCDE, aBcDe/etc.
        // \p{Lu} = any uppercase character, \p{Ll} = any lowercase character.
        $formats = array(
                '/^\\d*\\p{Lu}[\\p{Ll}\\d]*(?:\\W|$)/',
                '/^[\\p{Ll}\\d]+(?:\\W|$)/',
                '/^[\\p{Lu}\\d]+(?:\\W|$)/'
        );

        $keyed = array(); // Array of indexed values as lowercase => value
        foreach ($array as $value) {
            $value = '' . $value;
            $lcvalue = strtolower($value);
            if (isset($keyed[$lcvalue])) {
                // Choose the best value between current and stored.

                $prevvalue = $keyed[$lcvalue];
                if ($value == $prevvalue) {
                    // Same value, nothing to do.
                    continue;
                }
                foreach ($formats as $format) {
                    if (preg_match($format, $prevvalue)) {
                        // Currently stored value is at least as good as new value, keep it.
                        break;
                    }
                    if (preg_match($format, $value)) {
                        // New value is strictly better that stored value, store it.
                        $keyed[$lcvalue] = $value;
                        break;
                    }
                }
            } else {
                $keyed[$lcvalue] = $value;
            }
        }
        return array_values($keyed);
    }

    protected function update_record($fieldname, $suggestions) {
        global $DB;
        if (!empty($suggestions)) {
            if ($id = $DB->get_field('local_sharedspaceh_suggest', 'id', array('fieldname' => $fieldname))) {
                $DB->update_record('local_sharedspaceh_suggest', array(
                        'id' => $id,
                        'suggestions' => json_encode(array_values($suggestions))
                ));
            } else {
                $DB->insert_record('local_sharedspaceh_suggest', array(
                        'fieldname' => $fieldname,
                        'suggestions' => json_encode(array_values($suggestions))
                ));
            }
        } else {
            $DB->delete_records('local_sharedspaceh_suggest', array('fieldname' => $fieldname));
        }
    }

    protected function compute_cm_id_suggestions() {
        global $DB;
        // Already unique, no cleaning needed.
        return $DB->get_fieldset_sql(
                "SELECT instanceid
                   FROM {local_metadata}
                  WHERE fieldid = ? AND data = 1
               ORDER BY instanceid",
                array((int) get_config('local_sharedspaceh', 'metadatasharedfield')));
    }

    protected function compute_cm_type_suggestions() {
        global $DB;
        // Already unique, no cleaning needed.
        return $DB->get_fieldset_select('modules', "name", "visible = 1");
    }

    protected function compute_cm_name_suggestions() {
        global $DB;
        $modulestypes = $DB->get_records('modules', null, '', "id, name");
        $queries = array();
        $params = array();
        foreach ($modulestypes as $moduletype) {
            $queries[] = "(
                           SELECT cm.id, m.name
                             FROM {{$moduletype->name}} m
                             JOIN {course_modules} cm ON cm.instance = m.id AND cm.module = ?
                          )";
            $params[] = $moduletype->id;
        }
        $sql = "SELECT cm.name
                  FROM (" . implode("\nUNION ", $queries) . ") as cm
                  JOIN {local_metadata} lm ON lm.instanceid = cm.id
                 WHERE lm.fieldid = ? AND lm.data = 1
              GROUP BY cm.id";
        $params[] = (int) get_config('local_sharedspaceh', 'metadatasharedfield');

        return $this->clean_suggestions($DB->get_fieldset_sql($sql, $params));
    }

    protected function compute_mod_intro_suggestions() {
        global $DB;
        $modulestypes = $DB->get_records('modules', null, '', "id, name");
        $queries = array();
        $params = array();
        foreach ($modulestypes as $moduletype) {
            if (!plugin_supports('mod', $moduletype->name, FEATURE_MOD_INTRO)) {
                continue;
            }
            $queries[] = "(
                           SELECT cm.id, m.intro
                             FROM {{$moduletype->name}} m
                             JOIN {course_modules} cm ON cm.instance = m.id AND cm.module = ?
                          )";
            $params[] = $moduletype->id;
        }
        $sql = "SELECT cm.intro
                  FROM (" . implode("\nUNION ", $queries) . ") as cm
                  JOIN {local_metadata} lm ON lm.instanceid = cm.id
                 WHERE lm.fieldid = ? AND lm.data = 1
              GROUP BY cm.id";
        $params[] = (int) get_config('local_sharedspaceh', 'metadatasharedfield');

        return $this->clean_suggestions($DB->get_fieldset_sql($sql, $params), true);
    }

    protected function compute_course_name_suggestions() {
        global $DB;

        $coursenames = $DB->get_fieldset_sql(
                "SELECT c.fullname
                   FROM {course} c
                   JOIN {course_modules} cm ON cm.course = c.id
                   JOIN {local_metadata} lm ON lm.instanceid = cm.id AND lm.fieldid = ? AND lm.data = 1
               GROUP BY c.id",
                array((int) get_config('local_sharedspaceh', 'metadatasharedfield')));

        return $this->clean_suggestions($coursenames);
    }
}
