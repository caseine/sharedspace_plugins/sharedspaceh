<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh\metadata\import;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

class space_config_form extends \moodleform {

    public function definition() {
        $mform = $this->_form; // Don't forget the underscore!

        $mform->addElement('header', 'userguide', get_string('user_guide', 'local_sharedspaceh'));
        $mform->addElement('html', get_string('user_guide_content', 'local_sharedspaceh'));

        $mform->addElement('header', 'parsing', get_string('parsing_and_tagging_from_csv', 'local_sharedspaceh'));
        $mform->setExpanded('parsing');

        $maxbytes = 1000240;
        $mform->addElement('filepicker', 'csv_auto_tag_file', get_string('file_choose_one', 'local_sharedspaceh'), null,
                     array('maxbytes' => $maxbytes, 'accepted_types' => '.csv'));
        $mform->addRule('csv_auto_tag_file', null, 'required');

        $mform->addElement('select', 'csvseparator', get_string('csvseparator', 'local_sharedspaceh'),
                array(
                        ';' => get_string('csvseparator_semicolon', 'local_sharedspaceh') . ' (;)',
                        ',' => get_string('csvseparator_comma', 'local_sharedspaceh') . ' (,)',
                        "\t" => get_string('csvseparator_tab', 'local_sharedspaceh') . ' (\t)',
                        ' ' => get_string('csvseparator_space', 'local_sharedspaceh') . ' ( )'
                ));

        $mform->addElement('select', 'stringseparator', get_string('stringseparator', 'local_sharedspaceh'),
                array(
                        '"' => get_string('stringseparator_doublequote', 'local_sharedspaceh') . ' (")',
                        '\'' => get_string('stringseparator_simplequote', 'local_sharedspaceh') . ' (\')'
                ));

        $mform->addElement('select', 'fileencoding', get_string('fileencoding', 'local_sharedspaceh'),
                array(
                        'ISO-8859-1' => 'ISO-8859-1 (Latin 1)',
                        \Box\Spout\Common\Helper\EncodingHelper::ENCODING_UTF8 => 'UTF-8',
                        \Box\Spout\Common\Helper\EncodingHelper::ENCODING_UTF16_LE => 'UTF-16LE',
                        \Box\Spout\Common\Helper\EncodingHelper::ENCODING_UTF16_BE => 'UTF-16BE',
                        \Box\Spout\Common\Helper\EncodingHelper::ENCODING_UTF32_LE => 'UTF-32LE',
                        \Box\Spout\Common\Helper\EncodingHelper::ENCODING_UTF32_BE => 'UTF-32BE'
                ));

        $this->add_action_buttons(false, get_string('parse_and_load', 'local_sharedspaceh'));

    }
}
