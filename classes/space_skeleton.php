<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->dirroot.'/local/sharedspaceh/spacelib.php');

/**
 * A class to represent the common features of search forms of the sharedspace
 * @copyright 2018 Hadrien Cambazard, Nicolas Catusse
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class space_skeleton {

    private $fieldsuggestions;

    private $metadatafields;

    private $allfields = array();

    private $isfeaturedfield;

    public const QUERY_FIELD = 'query';

    public const CMID_FIELD = 'cm_id';
    public const CMNAME_FIELD = 'cm_name';
    public const COURSENAME_FIELD = 'course_name';
    public const CMTYPE_FIELD = 'cm_type';
    public const MODINTRO_FIELD = 'mod_intro';
    public const TIMECREATED_FIELD = 'timecreated';
    public const TIMEMODIFIED_FIELD = 'timemodified';

    public const STANDARD_MODULE_FIELDS = array(
            self::CMID_FIELD => array('matchingtype' => space_request::IN, 'collatingtype' => space_request::OR),
            self::CMNAME_FIELD => array('defaultmatchingtype' => space_request::LIKE, 'defaultcollatingtype' => space_request::OR),
            self::COURSENAME_FIELD => array('defaultmatchingtype' => space_request::LIKE, 'defaultcollatingtype' => space_request::OR),
            self::CMTYPE_FIELD => array('matchingtype' => space_request::IN, 'collatingtype' => space_request::OR, 'closedlist' => true),
            self::MODINTRO_FIELD => array('matchingtype' => space_request::LIKE, 'defaultcollatingtype' => space_request::AND),
            self::TIMECREATED_FIELD => array('type' => 'date'),
            self::TIMEMODIFIED_FIELD => array('type' => 'date')
    );

    public const METADATA_FIELD_PREFIX = 'md_';

    public const BEFORE = 1;
    public const AFTER = 2;
    public const BETWEEN = 3;
    public const EXACTDATE = 4;

    public function get_all_fields() {
        return $this->allfields;
    }

    public static function getOrderedMetadataFields($dbfields = "lmf.*") {
        global $DB;
        $sql = "SELECT $dbfields
                FROM {local_metadata_field} lmf
                JOIN {local_metadata_category} lmc ON lmc.id = lmf.categoryid
                WHERE lmf.contextlevel = ?
                ORDER BY lmc.sortorder, lmf.sortorder";
        return $DB->get_records_sql($sql, array(CONTEXT_MODULE));
    }

    public static function isMultipleValuesMetadataField($field) {
        return $field->datatype === 'autocomplete' && ($field->param1 ?? true);
    }

    public function __construct() {
        $this->prepareFields();
        $this->buildSuggestions();

        $featuredfields = get_config('local_sharedspaceh', 'metadatafeaturedfields');
        if (!empty($featuredfields)) {
            $this->isfeaturedfield = array_fill_keys(explode(',', $featuredfields), true);
        }
    }

    /**
     * Retrieve all the data necessary to create the widgets on the form.
     */
    private function prepareFields() {
        $this->allfields = array();
        $this->metadatafields = array();

        // Prepare metadata fields.
        foreach (static::getOrderedMetadataFields() as $field) {
            if (!$field->visible) {
                continue;
            }

            $this->metadatafields[] = $field;
            $this->allfields[] = self::METADATA_FIELD_PREFIX . $field->shortname;
        }

        // Prepare standard module fields.
        $nonmdfields = array_merge([self::QUERY_FIELD], array_keys(self::STANDARD_MODULE_FIELDS));
        foreach ($nonmdfields as $fieldid) {
            $this->allfields[] = $fieldid;
        }
    }

    private function buildSuggestions() {
        global $DB;
        $this->fieldsuggestions = $DB->get_records_menu('local_sharedspaceh_suggest', null, '', "fieldname, suggestions");
    }

    /**
     * Adds to the moodle form the free search with keywords (google type of search)
     * @mform: a moodle form to extend with a widget
     * @param $mform \MoodleQuickForm
     */
    public function buildFreeSearchWidget($mform) {
        $options = array(
            'placeholder' => 'Example: vpl python L2',
            'class' => 'main-query-field'
        );
        // TODO add suggestions of autocompletion for main query field.
        $icon = '<i class="fa fa-search searchbar-icon"
                    onclick="document.getElementsByName(\'' . self::QUERY_FIELD . '\')[0].focus();"></i>';
        $mform->addElement('html', $icon);
        $mform->addElement('text', self::QUERY_FIELD, '', $options);
        $mform->setType(self::QUERY_FIELD, PARAM_TEXT);
    }

    protected function addAdvancedSearchTitle($mform, $title, $advanced = false) {
        $header = $mform->addElement('static', uniqid(), '', \html_writer::tag('h4', $title));
        if ($advanced) {
            $mform->setAdvanced($header->getName(), $advanced);
        }
    }

    /**
     * Adds to the moodle form the free search with keywords (google type of search)
     * @mform: a moodle form to extend with a widget
     */
    public function buildFeaturedSearchWidget($mform) {
        foreach ($this->metadatafields as $field) {
            if (!empty($this->isfeaturedfield[$field->id])) {
                $this->addMetadataField($mform, $field, false, true);
            }
        }
    }

    /**
     * Adds to the moodle form the search through metadata criteria
     * @mform: a moodle form to extend with a widget
     */
    public function buildMetaDataSearchWidget($mform) {
        $options  = array(
            'multiple' => true,
            'tags' => true,
        );
        $this->addAdvancedSearchTitle($mform, get_string('filterbymetadata', 'local_sharedspaceh'));

        $this->addMetadataElements(true, $mform, $options);

        $this->addAdvancedSearchTitle($mform, get_string('filterbyadvancedmetadata', 'local_sharedspaceh'), true);

        $this->addMetadataElements(false, $mform, $options);
    }

    /**
     * Add ALL search widgets corresponding to meta data entries
     */
    private function addMetadataElements($default, $mform, $options) {
        $sharedfieldid = get_config('local_sharedspaceh', 'metadatasharedfield');
        foreach ($this->metadatafields as $field) {
            if ($field->id != $sharedfieldid && $field->defaultforsearch == $default && empty($this->isfeaturedfield[$field->id])) {
                $this->addMetadataField($mform, $field, !$default);
            }
        }
    }

    /**
     * return an ASSOCIATIVE array containing all the values existing in the database
     * for the field id $int_fieldid where the keys used are the values themselves
     */
    protected function get_auto_completion_values_map($fieldname) {
        if (empty($this->fieldsuggestions[$fieldname])) {
            return null;
        }

        return array_combine(json_decode($this->fieldsuggestions[$fieldname]), json_decode($this->fieldsuggestions[$fieldname]));
    }

    /**
     *
     * @param \MoodleQuickForm $mform
     * @param \local_metadata\fieldtype\metadata $item
     * @param boolean $featured
     * @param boolean $advanced
     */
    protected function addMetadataField($mform, $field, $advanced, $featured = false) {
        global $CFG;
        $fieldheader = format_string($field->name);
        $inputname = self::METADATA_FIELD_PREFIX . $field->shortname;

        if ($featured) {
            $this->addAdvancedSearchTitle($mform, get_string('filterby', 'local_sharedspaceh', $fieldheader), $advanced);
        }

        if ($field->datatype == 'checkbox') {
            $mform->addElement('select', $inputname, $fieldheader,
                    array(
                            '' => get_string('notspecified', 'local_sharedspaceh'),
                            '1' => get_string('yes'),
                            '0' => get_string('no')
                    ));
            $mformelementname = $inputname;
        } else if ($field->datatype == 'datetime') {
            $element = $this->addEnhancedDateField($mform, $inputname, $fieldheader);
            $mformelementname = $element->getName();
        } else {
            $completion = $this->get_auto_completion_values_map($inputname);
            $ismultiple = self::isMultipleValuesMetadataField($field);
            $element = $this->addEnhancedAutocompleteField($mform, $inputname, $fieldheader, $completion, array(), $ismultiple);
            $mformelementname = $element->getName();
        }

        $mform->setAdvanced($mformelementname, $advanced);

        if (!empty($field->description)) {
            // Use method from local_metadata to display the help button with description.
            require_once($CFG->dirroot . '/local/metadata/fieldtype/' . $field->datatype . '/classes/metadata.php');
            $fieldclass = "\\metadatafieldtype_{$field->datatype}\\metadata";
            $item = new $fieldclass($field->id);
            $item->edit_field_add_help_button($mform, $mformelementname);
        }
    }

    /**
     *
     * @param \MoodleQuickForm $mform
     * @param string $inputname
     * @param string $label
     * @param array $suggestions
     * @return \HTML_QuickForm_element
     */
    protected function addEnhancedAutocompleteField($mform, $inputname, $label, $suggestions, $options = array(), $ismultiplevaluesfield = false) {

        if (isset($options['matchingtype'])) {
            $options['defaultmatchingtype'] = $options['matchingtype'];
            $options['matchingtypelocked'] = true;
        }

        if (isset($options['collatingtype'])) {
            $options['defaultcollatingtype'] = $options['collatingtype'];
            $options['collatingtypelocked'] = true;
        }

        $elements = array();

        $elements[] =& $mform->createElement('select', 'matchingtype_' . $inputname, '',
                array(
                        space_request::LIKE => get_string('contain', 'local_sharedspaceh'),
                        space_request::IN => get_string($ismultiplevaluesfield ? 'match' : 'matchany', 'local_sharedspaceh')
                ),
                empty($options['matchingtypelocked']) ? array('class' => 'align-self-start') : array('class' => 'align-self-start', 'disabled' => 'disabled'));
        $mform->setDefault('matchingtype_' . $inputname, $options['defaultmatchingtype'] ?? space_request::LIKE);

        $elements[] =& $mform->createElement('select', 'collatingtype_' . $inputname, '',
                array(
                        space_request::AND => get_string('all', 'local_sharedspaceh'),
                        space_request::OR => get_string('any', 'local_sharedspaceh')
                ),
                empty($options['collatingtypelocked']) ? array('class' => 'align-self-start') : array('class' => 'align-self-start', 'disabled' => 'disabled'));
        $mform->setDefault('collatingtype_' . $inputname, $options['defaultcollatingtype'] ?? space_request::AND);

        if (!$ismultiplevaluesfield) {
            $mform->hideIf('collatingtype_' . $inputname, 'matchingtype_' . $inputname, 'eq', space_request::IN);
        }

        $elements[] =& $mform->createElement('html', \html_writer::span(get_string('of', 'local_sharedspaceh'), 'mt-2 mr-1 align-self-start'));

        $elements[] =& $mform->createElement('autocomplete', $inputname, $label, $suggestions,
                array(
                        'multiple' => true,
                        'tags' => empty($options['closedlist']),
                        'showsuggestions' => true,
                        'class' => 'inline-autocomplete'
                ));

        $group = $mform->addGroup($elements, null, $label);
        $group->updateAttributes(array('class' => 'inline-autocomplete-group'));

        return $group;
    }

    protected function addEnhancedDateField($mform, $inputname, $label) {
        $elements = array();
        $elements[] =& $mform->createElement('select', $inputname, '',
                array(
                        '' => get_string('notspecified', 'local_sharedspaceh'),
                        self::BEFORE => get_string('before', 'local_sharedspaceh'),
                        self::AFTER => get_string('after', 'local_sharedspaceh'),
                        self::BETWEEN => get_string('between', 'local_sharedspaceh'),
                        self::EXACTDATE => get_string('exactdate', 'local_sharedspaceh')
                ));
        $elements[] =& $mform->createElement('date_time_selector', 'date1_' . $inputname, '');
        $elements[] =& $mform->createElement('date_time_selector', 'date2_' . $inputname, '');
        $mform->hideIf('date1_' . $inputname, $inputname, 'eq', '');
        $mform->hideIf('date2_' . $inputname, $inputname, 'neq', self::BETWEEN);
        $group = $mform->addGroup($elements, null, $label, '<div style="flex-basis:100%"></div>');
        return $group;
    }

    /**
     * Adds to the moodle form the search through metadata criteria
     * @param \MoodleQuickForm $mform a moodle form to extend with a widget
     */
    public function buildMoodleIDSearchWidget($mform) {
        $this->addAdvancedSearchTitle($mform, get_string('filterbymodulefields', 'local_sharedspaceh'), true);

        foreach (self::STANDARD_MODULE_FIELDS as $fieldname => $options) {
            $label = get_string($fieldname . '_form', 'local_sharedspaceh');
            if (isset($options['type']) && $options['type'] == 'date') {
                $element = $this->addEnhancedDateField($mform, $fieldname, $label);
            } else {
                $completion = $this->get_auto_completion_values_map($fieldname);
                $element = $this->addEnhancedAutocompleteField($mform, $fieldname, $label, $completion, $options);
            }

            $mform->addHelpButton($element->getName(), $fieldname . '_form', 'local_sharedspaceh');
            $mform->setAdvanced($element->getName(), true);
        }

    }

}
