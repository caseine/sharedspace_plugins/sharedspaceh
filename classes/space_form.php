<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir.'/formslib.php');

/**
 * The form that is the root of the shared space
 */
class space_form extends \moodleform {

    public const WITH_MY_MODULES_FIELD = 'own';
    public const ALL_PLATFORM_FIELD = 'all';

    /**
     * A reference to the core data structure for shared space forms
     * @var space_skeleton $skeleton
     */
    protected $skeleton;

    protected function definition() {

        $mform = $this->_form;
        $this->skeleton = new space_skeleton();

        $mform->updateAttributes(array('class' => $mform->getAttribute('class') . ' cleanform searchform'));

        $this->skeleton->buildFreeSearchWidget($mform);

        $mform->addElement('header', 'advancedsearchheader', get_string('advancedsearch', 'search'));
        $mform->setExpanded('advancedsearchheader', false, true);

        $this->skeleton->buildFeaturedSearchWidget($mform);

        $this->skeleton->buildMetaDataSearchWidget($mform);

        $this->skeleton->buildMoodleIDSearchWidget($mform);

        $this->addSearchOptions();

        $this->add_action_buttons(false, get_string('search', 'local_sharedspaceh'));
    }

    protected function addSearchOptions() {
        $mform = $this->_form;

        $mform->addElement('header', 'searchoptionsheader', get_string('searchoptions', 'local_sharedspaceh'));
        $mform->setExpanded('searchoptionsheader', false, true);

        // Add checkbox to include the modules owned by the user.
        $mform->addElement('advcheckbox', self::WITH_MY_MODULES_FIELD, get_string('include_modules_search', 'local_sharedspaceh'));
        $mform->setDefault(self::WITH_MY_MODULES_FIELD, 0);
        $mform->addHelpButton(self::WITH_MY_MODULES_FIELD, 'include_modules_search', 'local_sharedspaceh');

        // Add checkbox to search all the modules on the platform - for admins only!
        if (has_capability('local/sharedspaceh:accessasadmin', \context_system::instance())) {
            $mform->addElement('advcheckbox', self::ALL_PLATFORM_FIELD, get_string('search_all_the_plateform', 'local_sharedspaceh'));
            $mform->setDefault(self::ALL_PLATFORM_FIELD, 0);
            $mform->addHelpButton(self::ALL_PLATFORM_FIELD, 'searchcheckbox', 'local_sharedspaceh');
            $mform->disabledIf(self::WITH_MY_MODULES_FIELD, self::ALL_PLATFORM_FIELD, 'checked');
        }
    }

    public function fill_data($dataarray) {
        $this->_form->updateSubmission($dataarray, array());
    }

    /**
     * @return space_request encapsulating the criteria chosen by user
     */
    public function generate_space_request() {

        $data = $this->get_data();

        if ($data === null) {
            throw new \moodle_exception('invalidformdata');
        }

        $request = new space_request();

        $request->set_search_all(!empty($data->{self::ALL_PLATFORM_FIELD}) && has_capability('local/sharedspaceh:accessasadmin', \context_system::instance()));
        $request->set_include_my_own_modules(!empty($data->{self::WITH_MY_MODULES_FIELD}));

        foreach ($this->skeleton->get_all_fields() as $field) {
            if (isset($data->{$field}) && ((is_array($data->{$field}) && count($data->{$field}) > 0) || (!is_array($data->{$field}) && $data->{$field} !== ''))) {
                $request->set_filter($field, $data->{$field});
                $filterinfo = array();
                foreach (array('matchingtype', 'collatingtype', 'date1', 'date2') as $infofield) {
                    if (isset($data->{$infofield . '_' . $field}) && $data->{$infofield . '_' . $field} !== null) {
                        $filterinfo[$infofield] = $data->{$infofield . '_' . $field};
                    }
                }
                $request->set_filterinfo($field, $filterinfo);
            }
        }

        return $request;
    }
}
