<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh\navigation;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/navigationlib.php');

class space_navigation_node extends \navigation_node {
    public function build_flat_navigation_list(\flat_navigation $nodes, $showdivider = false, $label = '') {
        parent::build_flat_navigation_list($nodes, $showdivider, $label);
        if (($flatnode = $nodes->find($this->key)) !== false && $flatnode instanceof \flat_navigation_node) {
            $flatnode->set_indent(1); // Indent this node in flat navigation.
        }
    }
}
