<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/tablelib.php');
require_once(__DIR__ . '/../spacelib.php');

use \html_writer;

/**
 * Class caseine_table
 *
 * @copyright 2018 Hadrien Cambazard, Nicolas Catusse
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class space_table extends \flexible_table {

    protected const PERPAGE = 30;

    public const CMID_COLUMN = 'cm_id';
    public const CMTYPE_COLUMN = 'cm_type';
    public const CMNAME_COLUMN = 'cm_name';
    public const COURSENAME_COLUMN = 'course_name';

    public const TIMECREATED_COLUMN = 'timecreated';
    public const TIMEMODIFIED_COLUMN = 'timemodified';

    public const METADATA_COLUMN_PREFIX = 'md_';

    public function init() {
        $this->setup_columns();
    }

    /**
     * Define the headers and id of columns.
     * Recall that:
     * $this->columns denotes the id of the columns
     * $this->headers denotes the name of the columns
     */
    protected function setup_columns() {
        $standardcolumns = array(
            'line' => '#',
            self::CMID_COLUMN => get_string('cm_id', 'local_sharedspaceh'),
            self::CMTYPE_COLUMN => get_string('cm_type', 'local_sharedspaceh'),
            self::CMNAME_COLUMN => get_string('activitymodule'),
            'cart' => get_string('addtocart', 'local_sharedspaceh'),
            'intro' => get_string('description'),
            self::COURSENAME_COLUMN => get_string('course')
        );
        if ($this->is_downloading()) {
            // These columns are not useful when downloading.
            unset($standardcolumns['line']);
            unset($standardcolumns['cart']);
            // This column is not relevant for metadata edition, and would be inconsistent because of carriage return removal.
            unset($standardcolumns['intro']);
        }

        $metadatacolumns = array();
        foreach (space_skeleton::getOrderedMetadataFields("lmf.shortname, lmf.name, lmf.visible") as $field) {
            if (!$field->visible) {
                continue;
            }
            $metadatacolumns[self::METADATA_COLUMN_PREFIX . $field->shortname] = format_string($field->name);
        }

        if (!$this->is_downloading()) {
            $standardcolumns2 = array(
                self::TIMECREATED_COLUMN => get_string('timecreated', 'local_sharedspaceh'),
                self::TIMEMODIFIED_COLUMN => get_string('timemodified', 'local_sharedspaceh'),
            );
        } else {
            $standardcolumns2 = array();
        }

        $columns = array_merge($standardcolumns, $metadatacolumns, $standardcolumns2);

        $this->define_columns(array_keys($columns));
        $this->define_headers($this->is_downloading() ? array_keys($columns) : array_values($columns));

        $this->collapsible(false);
        $this->column_class('line', 'nothideable');
        $this->column_class(self::CMNAME_COLUMN, 'nothideable');
        $this->column_class('cart', 'nothideable');

        $this->sortable(true);
        $this->no_sorting('line');
        $this->no_sorting('cart');
        $this->no_sorting('intro');
    }

    public static function column_needs_filters_applied_before_sorting($column) {
        return $column == self::CMNAME_COLUMN || $column == self::COURSENAME_COLUMN;
    }

    public function wrap_html_start() {
        $this->download_button();
        $this->columns_management_controls();
        parent::wrap_html_start();
    }

    protected function download_button() {
        $url = clone($this->baseurl);
        $url->param('download', 'csv');
        echo html_writer::div(html_writer::link($url, '<i class="fa fa-download mr-1"></i>' . get_string('downloadcsv', 'local_sharedspaceh')), 'my-1');
    }

    protected function columns_management_controls() {
        global $PAGE, $DB;
        $coldefaults = $DB->get_records_menu('local_metadata_field', array('contextlevel' => CONTEXT_MODULE), '', 'shortname, defaultforcolumns');

        $coloptions = array();
        foreach ($this->columns as $colid => $num) {
            $name = $this->headers[$num]; // By default, take the column header name.
            if (strpos($colid, self::METADATA_COLUMN_PREFIX) !== 0 && get_string_manager()->string_exists($colid . '_form', 'local_sharedspaceh')) {
                // We have a better name to describe the column.
                $name = get_string($colid . '_form', 'local_sharedspaceh');
            }
            $coloptions[] = html_writer::div($name, 'clickable py-1 px-2', array('data-colid' => $colid, 'data-colnum' => $num));

            $match = null;
            if (preg_match('/^' . self::METADATA_COLUMN_PREFIX . '(.+)/', $colid, $match)) {
                $mdname = $match[1];
            } else {
                $mdname = $colid;
            }
            $default = $coldefaults[$mdname] ?? 1;
            $visible = get_user_preferences('local_sharedspaceh_column_' . $colid, $default);
            if (!$visible) {
                $this->column_class($colid, 'hidden');
            }
        }

        $icon = '<i class="fa fa-plus mr-2"></i>';
        $button = '<span class="clickable position-absolute" role="add-table-columns" style="display:none;bottom:0;right:0;">' . $icon . get_string('columns', 'local_sharedspaceh') . '</span>';
        echo html_writer::div($button, 'position-relative');
        $menu = '<div class="bg-white position-absolute border" role="table-columns-menu" style="display:none">' .
                    implode('', $coloptions) .
                '</div>';
        echo html_writer::div($menu, 'position-relative');

        $PAGE->requires->string_for_js('hidecolumn', 'local_sharedspaceh');
        $PAGE->requires->js_call_amd('local_sharedspaceh/manage_space_columns', 'init', array(array_flip($this->columns)));
    }

    /**
     * Fill the table with one row for each module
     */
    public function fill_caseine_table(array $results) {
        $this->pagesize(self::PERPAGE, count($results));

        if (!$this->is_downloading()) {
            // Slice for paging.
            $results = array_slice($results, $this->get_page_start(), self::PERPAGE);
        } else {
            // Filter out modules the user cannot edit.
            $results = array_filter($results, function($result) {
                return has_capability('moodle/course:manageactivities', \context_module::instance($result->instanceid));
            });
        }

        $rows = array();
        $before = microtime(true);
        foreach (array_values($results) as $i => $result) {
            $cm = get_fast_modinfo($result->courseid)->get_cm($result->instanceid);
            $row = $this->add_cm_line($cm);
            $row['line'] = $i + 1 + $this->get_page_start();
            $rows[] = $row;
        }
        $seconds = format_float(microtime(true) - $before, 4);
        if (!$this->is_downloading()) {
            echo html_writer::div(get_string('processingtime', 'local_sharedspaceh', array('seconds' => $seconds)), 'small');
        }

        foreach ($rows as $row) {
            $this->add_data_keyed($row);
        }
    }

    /**
     * Add a given course module $cm at at the line number $count of the table
     * @param int $count : index of a line in the table
     * @param \cm_info $cm : a course module
     */
    private function add_cm_line($cm) {
        global $DB;
        $row = array(
            self::CMID_COLUMN => $cm->id,
            self::CMTYPE_COLUMN => $this->is_downloading() ? $cm->modname : $this->html_modtype($cm),
            self::CMNAME_COLUMN => $this->is_downloading() ? $cm->get_formatted_name() : $this->html_module($cm),
            'cart' => $this->is_downloading() ? null : $this->html_cart($cm),
            'intro' => $this->get_module_intro($cm, !$this->is_downloading()),
            self::COURSENAME_COLUMN => $this->is_downloading() ? format_string($cm->get_course()->fullname) : $this->html_course($cm->get_course())
        );

        $dataoutput = new \metadatacontext_module\output\manage_data($cm);
        foreach ($dataoutput->data as $items) {
            unset($items['categoryname']);

            foreach ($items as $item) {

                if (!$item->field->visible) {
                    continue;
                }

                if ($this->is_downloading()) {
                    $value = $item->data;
                } else {
                    $value = $item->display_data();

                    if (!empty($item->data) && $item->data == $item->field->defaultdata) {
                        // Data is identical to default data, check if it is real or default data.
                        if (!$DB->record_exists('local_metadata', array('instanceid' => $cm->id, 'fieldid' => $item->fieldid))) {
                            $value = '<em title="' . get_string('thisisdefault', 'local_sharedspaceh') . '">' . $value . '</em>';
                        }
                    }

                    $pos = strpos($value, "#>");
                    if ($pos !== false) {
                        $hover = substr($value, $pos + 2);
                        $val = substr($value, 0, $pos);
                        $value = '<span title="' . $hover . '">' . $val . '</span>';
                    }

                }

                $row[self::METADATA_COLUMN_PREFIX . $item->field->shortname] = $value;

            }
        }

        $row[self::TIMECREATED_COLUMN] = $this->is_downloading() ? $cm->added : userdate($cm->added);
        $timemodified = $DB->get_record($cm->modname, array('id' => $cm->instance))->timemodified ?? 0;
        $row[self::TIMEMODIFIED_COLUMN] = $this->is_downloading() ? $timemodified : ($timemodified ? userdate($timemodified) : get_string('notset', 'profilefield_datetime'));

        return $row;
    }

    /**
     *
     * @param \cm_info $cm
     * @param boolean $format
     * @return string
     */
    protected function get_module_intro($cm, $format = true) {
        global $DB;
        if (plugin_supports('mod', $cm->modname, FEATURE_MOD_INTRO)) {
            $record = $DB->get_record($cm->modname, array('id' => $cm->instance), 'intro, introformat');
            $description = $record->intro;
            if ($format) {

                $description = format_module_intro($cm->modname, $record, $cm->id);

                // Remove script tags.
                if ($description > '') {
                    $dom = new \DOMDocument();
                    $dom->loadHTML(utf8_decode($description), LIBXML_NOERROR | LIBXML_NOWARNING); // Do not display errors for broken HTML.
                    foreach ($dom->getElementsByTagName('script') as $script) {
                        $script->parentNode->removeChild($script);
                    }
                    $description = trim($dom->saveHTML());
                }

                $limit = 150;
                $preview = strip_tags($description, '<br>');
                if (strlen($preview) > $limit) {
                    $preview = substr($preview, 0, $limit);
                    $linkaction = 'event.preventDefault();this.nextSibling.style.display=\'\';this.previousSibling.remove();this.remove();';
                    $description = '<span>' . $preview . '</span><a href="#" onclick="' . $linkaction . '">' .
                            '&hellip;&nbsp;(' . get_string('showallintro', 'local_sharedspaceh') . ')' .
                            '</a>' .
                            '<div style="display:none;">' . $description . '</div>';
                }

            }
            return $description;
        } else {
            return '';
        }
    }

    private function html_modtype($cm) {
        global $OUTPUT;
        return $OUTPUT->pix_icon('icon', $cm->get_module_type_name(), $cm->modname, array('class' => 'iconlarge activityicon'));
    }

    /**
     * @param \cm_info $cm
     * @return string
     */
    private function html_module($cm) {
        global $DB;

        $text = $cm->get_formatted_name();

        if ($cm->url !== null) {
            // The access checks here may not be exact.
            // It is fine, this is just to decide whether we put a link; modules will check for actual security on their side.
            $modcontext = \context_module::instance($cm->id);
            $canmanage = has_capability('moodle/course:manageactivities', $modcontext);
            $candirectview = $cm->uservisible && is_enrolled(\context_course::instance($cm->course));
            $viewcap = 'mod/' . $cm->modname . ':view';
            $canguestview = $DB->record_exists('enrol', [ 'courseid' => $cm->course, 'enrol' => 'guest', 'status' => ENROL_INSTANCE_ENABLED ])
                    && get_capability_info($viewcap) != null && has_capability($viewcap, $modcontext, guest_user());
            if ($canmanage || $candirectview || $canguestview) {
                $text = html_writer::link($cm->url, $text, !$cm->visible ? array('class' => 'dimmed') : null);
            }
        }

        $originalcmidfield = get_config('local_sharedspaceh', 'metadataoriginalcmidfield');
        if (!empty($originalcmidfield)) {
            // Count how many times the module has been copied (by metadataoriginalcmidfield metadata).
            $sql = "SELECT COUNT(*) as n, COUNT(DISTINCT cm.course) as c
                FROM {local_metadata} m
                JOIN {course_modules} cm ON cm.id = m.instanceid
                WHERE m.fieldid = :fieldid AND m.data = :cmid AND m.instanceid <> m.data";
            $res = $DB->get_record_sql($sql, array('fieldid' => $originalcmidfield, 'cmid' => $cm->id));
            if ($res->c >= 2) {
                $copyinfotext = get_string('thisitemispopular', 'local_sharedspaceh', $res);
                $copyinfo = '<i class="fa fa-star-o position-absolute mt-1" style="margin-left: -10px; color: goldenrod" title="' . $copyinfotext . '"></i>';
                $text = html_writer::div(html_writer::span($text, 'pr-3') . $copyinfo, 'position-relative');
            }
        }

        return $text;
    }

    private function html_course($course) {
        global $OUTPUT;

        // Fancy course name display.
        $courseicon = html_writer::img($OUTPUT->image_url('i/course'), get_string('course'), array('class' => 'mr-2', 'style' => 'vertical-align:sub;'));
        $courseurl = \context_course::instance($course->id)->get_url();

        $html = html_writer::div(html_writer::link($courseurl, $courseicon . html_writer::span(format_string($course->fullname), 'course-name'), array('class' => 'course-link')));

        // Add enrolment icons.
        if ($icons = enrol_get_course_info_icons($course)) {
            $renderedicons = implode('', array_map(array($OUTPUT, 'render'), $icons));
            $html .= html_writer::div($renderedicons, 'float-left ml-3 pl-1', array('style' => 'transform: scale(85%);'));
        }
        return $html;
    }

    /**
     *
     */
    private function html_cart($cm) {
        // Add a temporary role that allows to backup.
        \local_sharedspaceh_assign_temporary_backuper_role(\context_module::instance($cm->id));

        // Mimic a course, so the Sharing Cart behaves like we are in an actual course.
        return
        '<ul class="course-content list-inline">
            <li id="section-1" class="section" data-courseid="' . $cm->course . '">
                <ul class="list-inline">
                    <li id="module-' . $cm->id . '" class="activity bg-transparent">
                        <div class="actions position-static">
                            <div class="action-menu section-cm-edit-actions">
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>';
    }

}
