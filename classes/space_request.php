<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_sharedspaceh;

/**
 * A class to represent a request made in the search space
 * This class provides an easy access to the field/criteria selected by the user
 * and is able to execute the corresponding request in sql.
 *
 * @copyright 2018 Hadrien Cambazard, Nicolas Catusse
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class space_request {

    /**
     * (boolean)
     *  = 1 if the search is performed over the plateform
     */
    private $searchall;

    /**
     * (boolean)
     * = 1 if the search should also show the activities the user is allowed to edit/manage
     */
    private $includemymodules;

    /**
     * (String[][])
     * $criteria_metadata[i] is an array of string containing the values chosen for the i-th metadata
     * i denotes the id of the metadata field in the corresponding table local_metadata_field
     */
    private $metadatafieldcriteria;

    private $standardmodulecriteria;

    private $fieldfilterinfo;

    /**
     * (String[])
     * An array of string containing the values chosen as free keyword in the free search page
     */
    private $freekeywords;

    public const LIKE = 'LIKE';
    public const IN = 'IN';
    public const AND = 'AND';
    public const OR = 'OR';

    public function __construct() {
        $this->searchall = false;
        $this->metadatafieldcriteria = null;
        $this->standardmodulecriteria = null;
        $this->fieldfilterinfo = array();
    }

    // **************************************** //
    // *** Set the criteria of the requests *** //
    // **************************************** //

    /**
     * MUST be called before running the request to define the scope.
     * @param bool $search_all: true when search should be performed over all the plateform
     */
    public function set_search_all(bool $searchall) {
        $this->searchall = $searchall;
    }

    /**
     * Set the boolean indicating wether the modules that the user has rights to manage
     * should be included or not in the result
     */
    public function set_include_my_own_modules(bool $includemymodules) {
        $this->includemymodules = $includemymodules;
    }

    /**
     * Set an array of conditions for the corresponding search field
     * @param string $fieldid: the identifier of the search field
     * @param mixed $values: array or string of values
     */
    public function set_filter(string $fieldid, $values) {
        if ($fieldid == space_skeleton::QUERY_FIELD) {
            $this->freekeywords = $values;
        } else if (isset(space_skeleton::STANDARD_MODULE_FIELDS[$fieldid])) {
            $this->standardmodulecriteria[$fieldid] = $values;
        } else if (strpos($fieldid, space_skeleton::METADATA_FIELD_PREFIX) === 0) {
            $this->metadatafieldcriteria[$fieldid] = $values;
        }
    }

    public function set_filterinfo($fieldid, $filterinfo) {
        $this->fieldfilterinfo[$fieldid] = $filterinfo;
    }

    protected function get_filters() {
        $filters = array();
        if (!empty($this->freekeywords)) {
            $filters[space_skeleton::QUERY_FIELD] = $this->freekeywords;
        }

        if (!empty($this->standardmodulecriteria)) {
            $filters = array_merge($filters, $this->standardmodulecriteria);
        }

        if (!empty($this->metadatafieldcriteria)) {
            $filters = array_merge($filters, $this->metadatafieldcriteria);
        }
        return $filters;
    }

    public function to_url_params() {
        $params = array();

        foreach ($this->get_filters() as $fid => $values) {
            $fnamebar = str_replace(' ', '', $fid);
            if (is_array($values)) {
                for ($i = 0; $i < count($values); $i++) {
                    $params[$fnamebar . '[' . $i . ']'] = $values[$i];
                }
            } else {
                $params[$fnamebar] = $values;
            }

            if (isset($this->fieldfilterinfo[$fid])) {
                foreach ($this->fieldfilterinfo[$fid] as $infofield => $info) {
                    $params[$infofield . '_' . $fnamebar] = $info;
                }
            }
        }

        if ($this->searchall) {
            $params[space_form::ALL_PLATFORM_FIELD] = 1;
        } else if ($this->includemymodules) {
            $params[space_form::WITH_MY_MODULES_FIELD] = 1;
        }

        return $params;
    }

    public function get_form_data() {
        $data = array();
        if (!empty($this->metadatafieldcriteria)) {
            $data = array_merge($data, $this->metadatafieldcriteria);
        }
        if (!empty($this->standardmodulecriteria)) {
            $data = array_merge($data, $this->standardmodulecriteria);
        }
        foreach ($this->fieldfilterinfo as $fid => $filterinfo) {
            foreach ($filterinfo as $infofield => $info) {
                $data[$infofield . '_' . $fid] = $info;
            }
        }
        $data[space_skeleton::QUERY_FIELD] = $this->freekeywords;
        $data[space_form::ALL_PLATFORM_FIELD] = $this->searchall;
        $data[space_form::WITH_MY_MODULES_FIELD] = $this->includemymodules;

        return (object)$data;
    }

    /**
     * @return true if some search conditions have been set on modules id
     */
    protected function is_module_filter_active() {
        return !is_null($this->standardmodulecriteria);
    }

    /**
     * @return true if some search conditions have been set on modules metadata
     */
    protected function is_field_filter_active() {
        return !is_null($this->metadatafieldcriteria);
    }

    /**
     * Easy description of the request
     */
    public function print() {
        if (!empty($this->searchall)) {
            echo "Searching all platform<br>";
        }
        if (!empty($this->metadatafieldcriteria)) {
            echo "Metadata filter: " . json_encode($this->metadatafieldcriteria) . '<br>';
        }
        if (!empty($this->standardmodulecriteria)) {
            echo "Standard module fields filter: " . json_encode($this->standardmodulecriteria) . '<br>';
        }
        if (!empty($this->freekeywords)) {
            echo "Free search keywords filter: " . json_encode($this->freekeywords) . '<br>';
        }
        echo '</br>';
    }

    /********************************************************/
    /****************** Search ******************************/
    /********************************************************/

    /**
     * Constructs a SQL query that retrieves all modules names and description.
     * Query returns 4 fields: cmid as id, name, intro, timemodified.
     *
     * Usage: list($sql, $params) = construct_subquery_for_modules_names_and_intro()
     * @return array A list consisting of the SQL fragment and the array of parameters.
     */
    protected function construct_subquery_for_modules_names_and_intro() {
        global $DB;
        $modulestypes = $DB->get_records_sql("SELECT id, name FROM {modules}");
        $queries = array();
        $params = array();
        foreach ($modulestypes as $moduletype) {
            $fields = "cm.id, m.name";
            if (plugin_supports('mod', $moduletype->name, FEATURE_MOD_INTRO)) {
                $fields .= ", m.intro";
            } else {
                $fields .= ", '' as intro";
            }
            if ($DB->get_manager()->field_exists($moduletype->name, 'timemodified')) {
                $fields .= ", m.timemodified";
            } else {
                $fields .= ", NULL as timemodified";
            }
            $queries[] = "(SELECT $fields FROM {{$moduletype->name}} m JOIN {course_modules} cm ON cm.instance = m.id AND cm.module = ?)";
            $params[] = $moduletype->id;
        }
        return array(implode("\nUNION ", $queries), $params);
    }

    /**
     * Constructs SQL fragments used for sorting.
     *
     * Usage: list($sqljoin, $selectcolumns, $postprocesscolumns) = construct_sql_for_sorting($table->get_sort_columns())
     *
     * @param array $sortcolumns The result of calling get_sort_columns() on the table.
     * @return array A list consisting of an SQL JOIN fragment,
     *  an associative array of column => alias to include in select statement,
     *  and an array of columns (alias) that will need postprocessing before sorting.
     */
    protected function construct_sql_for_sorting($sortcolumns) {
        $join = "";
        $forselect = array();
        $postprocesscolumns = array();

        foreach (array_keys($sortcolumns) as $i => $sortcolumn) {
            // We know that $sortcolumn can be trusted.
            $match = null;
            if (preg_match('/^' . space_table::METADATA_COLUMN_PREFIX . '(.+)/', $sortcolumn, $match)) {
                // Sort according to a metadata field.
                // Use a JOIN for this particular field so we have a dedicated column to sort on.
                $mdname = $match[1];
                $lmfalias = "lmf" . ($i + 3); // Tables lmf and lmf2 already exist in the main query.
                $lmalias = "lm" . ($i + 3); // Tables lm and lm2 already exist in the main query.
                $join .= "LEFT JOIN {local_metadata_field} $lmfalias ON $lmfalias.shortname = '$mdname'
                         LEFT JOIN {local_metadata} $lmalias ON $lmalias.instanceid = cm.id AND $lmalias.fieldid = $lmfalias.id
                         ";
                $forselect["$lmalias.data"] = $sortcolumn;
            } else {
                // Sort according to a standard module field.
                // Possible columns are: cm_id, cm_type, cm_name, course_name, timecreated, timemodified.
                // No need for an additional join, data is already retrieved in main query.
                $subjectfield = array(
                        space_table::CMID_COLUMN => 'cm.id',
                        space_table::CMTYPE_COLUMN => 'm.name',
                        space_table::CMNAME_COLUMN => 'module.name',
                        space_table::COURSENAME_COLUMN => 'c.fullname',
                        space_table::TIMECREATED_COLUMN => 'cm.added',
                        space_table::TIMEMODIFIED_COLUMN => 'module.timemodified'
                );
                if (space_table::column_needs_filters_applied_before_sorting($sortcolumn)) {
                    // For these columns, we want to sort after applying filters.
                    $postprocesscolumns[] = $sortcolumn;
                }
                $forselect[$subjectfield[$sortcolumn]] = $sortcolumn;
            }
        }
        return array($join, $forselect, $postprocesscolumns);
    }

    /**
     * Constructs SQL fragments used to apply filters on modules metadata.
     *
     * Usage: list($sqljoin, $sqlwhere, $whereparams, $groupbyclause) = construct_sql_for_metadata_filters()
     *
     * @return array A list consisting of an SQL JOIN fragment,
     *  an SQL WHERE fragment,
     *  an array of SQL parameters for the WHERE fragment,
     *  and an SQL fragment to add to the GROUP BY clause.
     */
    protected function construct_sql_for_metadata_filters() {
        global $DB;

        $join = "";
        $where = "";
        $params = array();
        $groupby = "";

        if ($this->is_field_filter_active()) {
            $metadatafields = $DB->get_records('local_metadata_field', array('contextlevel' => CONTEXT_MODULE));
            $metadatafields = array_combine(array_column($metadatafields, 'shortname'), $metadatafields);

            $wheres = array();
            foreach ($this->metadatafieldcriteria as $key => $values) {
                if (!is_array($values)) {
                    $values = [$values];
                }
                if (count($values) == 0) {
                    continue;
                }
                $match = null;
                if (preg_match('/^' . space_skeleton::METADATA_FIELD_PREFIX . '(.+)/', $key, $match)) {
                    // This condition should always be verified. We still use preg_match to retrieve the suffix.

                    $fieldname = $match[1];

                    if ($metadatafields[$fieldname]->datatype == 'datetime') {
                        // Date selector.
                        $date1 = $this->fieldfilterinfo[$key]['date1'] ?? 0;
                        $date2 = $this->fieldfilterinfo[$key]['date2'] ?? 0;
                        list($w, $p) = $this->get_date_field_where_clause("lmx.data", reset($values), $date1, $date2);
                    } else {
                        // Determine whether we search in a single value or a comma-separated list of values.
                        $iscommaseparatedvalues = space_skeleton::isMultipleValuesMetadataField($metadatafields[$fieldname]);

                        $matchingtype = $this->fieldfilterinfo[$key]['matchingtype'] ?? self::LIKE;
                        $collatingtype = $this->fieldfilterinfo[$key]['collatingtype'] ?? self::AND;
                        list($w, $p) = $this->get_field_criterion_where_clause($values, "lmx.data", $matchingtype, $collatingtype, $iscommaseparatedvalues);
                    }

                    $wheres[] = "(lmfx.shortname=? AND ($w))";
                    $params = array_merge($params, array($fieldname), $p);
                }
            }

            if ($wheres) {
                $join = "JOIN {local_metadata} lmx ON lmx.instanceid = cm.id
                    JOIN {local_metadata_field} lmfx ON lmfx.id = lmx.fieldid";
                // We join the conditions with an OR and group by with a HAVING COUNT(DISTINCT)
                // so at the end we only end up with cm.ids with matching in every criteria.
                // This is basically a big AND.
                $where = implode(" OR ", $wheres);
                $groupby = " HAVING COUNT(DISTINCT lmx.fieldid) = " . count($wheres);
            }
        }

        return array($join, $where, $params, $groupby);
    }

    /**
     * Constructs SQL fragments used to apply filters on standard module fields.
     *
     * Usage: list($sqlwhere, $whereparams) = construct_where_clause_for_standard_module_filters()
     *
     * @return array A list consisting of an SQL WHERE fragment and an array of SQL parameters.
     */
    protected function construct_where_clause_for_standard_module_filters() {
        $where = "";
        $params = array();

        if ($this->is_module_filter_active()) {

            // Mapping of Form field => SQL field(s).
            $moodlefields = array(
                    space_skeleton::CMID_FIELD => 'cm.id',
                    space_skeleton::CMTYPE_FIELD => 'm.name',
                    space_skeleton::CMNAME_FIELD => 'module.name',
                    space_skeleton::COURSENAME_FIELD => array('c.fullname', 'c.shortname'), // Search in any of these fields.
                    space_skeleton::MODINTRO_FIELD => 'module.intro',
                    space_skeleton::TIMECREATED_FIELD => 'cm.added',
                    space_skeleton::TIMEMODIFIED_FIELD => 'module.timemodified'
            );

            $wheres = array();
            foreach ($moodlefields as $field => $sqlfields) {
                if (!empty($this->standardmodulecriteria[$field])) {
                    if ((space_skeleton::STANDARD_MODULE_FIELDS[$field]['type'] ?? 'standard') === 'date') {
                        $date1 = $this->fieldfilterinfo[$field]['date1'] ?? 0;
                        $date2 = $this->fieldfilterinfo[$field]['date2'] ?? 0;
                        list($w, $p) = $this->get_date_field_where_clause($sqlfields, $this->standardmodulecriteria[$field], $date1, $date2);
                    } else {
                        // Matching and collating types defined in space_skeleton have precedence.
                        $matchingtype = space_skeleton::STANDARD_MODULE_FIELDS[$field]['matchingtype'] ?? ($this->fieldfilterinfo[$field]['matchingtype'] ?? self::LIKE);
                        $collatingtype = space_skeleton::STANDARD_MODULE_FIELDS[$field]['collatingtype'] ?? ($this->fieldfilterinfo[$field]['collatingtype'] ?? self::AND);
                        list($w, $p) = $this->get_field_criterion_where_clause($this->standardmodulecriteria[$field], $sqlfields, $matchingtype, $collatingtype);
                    }
                    $wheres[] = $w;
                    $params = array_merge($params, $p);
                }
            }
            $where = implode(" AND ", $wheres);
        }
        return array($where, $params);
    }

    /**
     * Execute the request specified by user.
     * This dynamically build a complex single SQL query.
     *
     * @param array $sortcolumns The result of calling get_sort_columns() on the table.
     * @param string $sqlsort The result of calling get_sql_sort() on the table.
     * @return array An array of course modules.
     */
    public function run($sortcolumns = array(), $sqlsort = "") {
        global $DB;

        // Build a query getting cmid => cm name / intro to allow search on module name and intro.
        list($modulenamesquery, $modulenamesparams) = $this->construct_subquery_for_modules_names_and_intro();

        // Build the part of the query that will be used to sort the results.
        list($sortjoin, $sortforselect, $postprocesscolumns) = $this->construct_sql_for_sorting($sortcolumns);
        $sortselect = "";
        $sortglobalselect = "";
        foreach ($sortforselect as $column => $alias) {
            $sortselect .= ", $column as $alias";
            $sortglobalselect .= ", $alias";
        }

        // Build the part of the query to filter results on metadata fields.
        list($metadatafiltersjoin, $metadatafilterswhere,
                $metadatafilterswhereparams, $groupbyclause) = $this->construct_sql_for_metadata_filters();

        // Build the part of the query to filter results on standard module fields.
        // This is just adding conditions to the inner WHERE.
        list($standardfilterswhere, $standardfiltersparams) = $this->construct_where_clause_for_standard_module_filters();

        // Concatenate filters into the WHERE clause.
        $innerwheres = array_filter(array($metadatafilterswhere, $standardfilterswhere), 'strlen');
        $innerwheresparams = array_merge($metadatafilterswhereparams, $standardfiltersparams);
        $innerwhere = $innerwheres ? "WHERE " . implode(" AND ", $innerwheres) : "";

        $sharedfieldid = (int) get_config('local_sharedspaceh', 'metadatasharedfield');
        $paramsql = array_merge(array($sharedfieldid), $modulenamesparams, $innerwheresparams);

        $freesearchkeywords = $this->process_freewords();
        if (!empty($freesearchkeywords)) {
            // Concatenate data on which free search will be performed.
            $searchfields = array("m.name", "cm.id", "module.name", "module.intro", "c.fullname", "c.shortname", "c.id");
            $fieldsselect = implode(",',',", $searchfields);
            $dataconcatselect = ", CONCAT($fieldsselect,COALESCE(CONCAT(',',GROUP_CONCAT(lm.data)), '')) as data_concat";
        } else {
            $dataconcatselect = "";
        }

        // Main query.
        // Here we just join needed standard data with one double JOIN on metadata.
        // This double JOIN is here to do a general concat on metadata with lm,
        // and manage shared modules with lm2 (either restrict to shared modules or retrieve shared status).
        $sql = "SELECT cm.id as instanceid, COALESCE(lm2.data, 0) as shared, c.id as courseid $sortselect $dataconcatselect
                FROM {course_modules} cm
                JOIN {course} c on c.id = cm.course
                JOIN {modules} m ON m.id = cm.module
                " . (
                    $this->searchall || $this->includemymodules ?
                    "LEFT JOIN {local_metadata} lm ON lm.instanceid = cm.id
                    LEFT JOIN {local_metadata} lm2 ON lm2.instanceid = cm.id AND lm2.fieldid = ?"
                    :
                    "JOIN {local_metadata} lm ON lm.instanceid = cm.id
                    JOIN {local_metadata} lm2 ON lm2.instanceid = cm.id AND lm2.fieldid = ? AND lm2.data = 1"
                ) . "
                $metadatafiltersjoin
                $sortjoin
                JOIN ($modulenamesquery) as module ON module.id = cm.id
                $innerwhere
                GROUP BY cm.id $groupbyclause";

        if (!empty($freesearchkeywords)) {
            // Wrap the query to allow WHERE LIKE in concatenated data.
            list($sqlwhere, $params) = $this->get_field_criterion_where_clause($freesearchkeywords, 'data_concat');
            $sql = "SELECT instanceid, shared, courseid $sortglobalselect
                    FROM ($sql) modules_data
                    WHERE $sqlwhere";
            $paramsql = array_merge($paramsql, $params);
        }

        $requiresprocessingbeforesort = !empty($postprocesscolumns);

        if (!$requiresprocessingbeforesort) {
            // We can directly order by.
            if ($sqlsort) {
                $sql .= "\nORDER BY $sqlsort";
            }
        }

        // Debug: uncomment the following lines to display the query.
        //$i = 0;
        //echo nl2br(preg_replace_callback('/\?/', function($matches) use (&$i,$paramsql) {
        //    $i++; return json_encode($paramsql[$i-1]);
        //}, $sql));

        // Perform the query.
        $results = $DB->get_records_sql($sql, $paramsql);

        $this->postprocess_and_filter_results($results, $postprocesscolumns);

        if ($requiresprocessingbeforesort) {
            // Sorting has not been done by SQL, so we need to sort ourselves.
            foreach (array_reverse($sortcolumns) as $sortcolumn => $sortorder) {
                $sortvalues = array_column($results, strtolower($sortcolumn));
                array_multisort($sortvalues, $sortorder, $results);
            }

        }

        // We cannot limit from SQL nor slice here because the paging in the table requires
        // to know how many rows there are in total...
        return $results;
    }

    /**
     *
     * @param array $results
     * @param array $postprocesscolumns Columns for which the values should go through format_string before sorting.
     */
    protected function postprocess_and_filter_results(&$results, $postprocesscolumns) {

        if ($this->includemymodules && !$this->searchall) {
            // Filter out modules that are not accessible.
            $results = array_filter($results, function($result) {
                return $result->shared || has_capability('moodle/course:manageactivities', \context_module::instance($result->instanceid));
            });
        }

        // Apply filters on some columns so we can then sort on the filtered text.
        foreach ($postprocesscolumns as $postprocesscolumn) {
            $results = array_map(function($result) use ($postprocesscolumn) {
                $result->{$postprocesscolumn} = format_string($result->{$postprocesscolumn});
                return $result;
            }, $results);
        }

    }

    /**
     * Process the main query field, allowing special commands.
     * @return array An array of kept keywords.
     */
    protected function process_freewords() {
        // Allow query to contain quotes to force group some words.
        $matches = array();
        preg_match_all('#[^" ]+|"[^"]+"#', $this->freekeywords, $matches, PREG_PATTERN_ORDER);
        $keywords = array_map(function($kw) {
            return trim($kw, ' "');
        }, $matches[0]);
        return $keywords;
    }

    /**
     * Constructs SQL WHERE clause for a filter on a date.
     *
     * @param string $sqlfield SQL column name to compare.
     * @param mixed $comparisontype One of space_skeleton::BEFORE/AFTER/BETWEEN/EXACTDATE.
     * @param number $date1 Date to compare.
     * @param number $date2 Secondary date to compare (only with BETWEEN).
     * @return array A list consisting of an SQL WHERE fragment and an array of parameters.
     */
    protected function get_date_field_where_clause($sqlfield, $comparisontype, $date1, $date2 = 0) {
        $where = "";
        $params = array();
        switch ($comparisontype) {
            case space_skeleton::BEFORE:
                $where = "$sqlfield <= ? AND $sqlfield > 0"; // Date 0 means not set.
                $params = array($date1);
                break;
            case space_skeleton::AFTER:
                $where = "$sqlfield >= ?";
                $params = array($date1);
                break;
            case space_skeleton::EXACTDATE:
                $where = "$sqlfield = ?";
                $params = array($date1);
                break;
            case space_skeleton::BETWEEN:
                $where = "$sqlfield >= ? AND $sqlfield <= ?";
                $params = array(min($date1, $date2), max($date1, $date2));
                break;
        }
        return array($where, $params);
    }

    /**
     * Constructs SQL WHERE clause for a filter on a text field.
     *
     * @param array $values Filter values.
     * @param string|array $sqlfields SQL column name to compare to. If several are given, it means search in any of these columns.
     * @param mixed $comparisontype One of self::LIKE/IN.
     * @param mixed $collatingtype One of self::AND/OR.
     * @param boolean $oncommaseparatedvalues Whether values of the SQL column we are searching on must be considered as lists.
     * @return array An array consisting of an SQL WHERE fragment and an array of parameters.
     */
    protected function get_field_criterion_where_clause($values, $sqlfields, $comparisontype = self::LIKE, $collatingtype = self::AND, $oncommaseparatedvalues = false) {
        global $DB;

        if (!is_array($sqlfields)) {
            $sqlfields = [$sqlfields];
        }

        if ($comparisontype == self::IN) {

            if ($oncommaseparatedvalues) {
                // Request is: precisely match one or more values in the comma-separated list.
                // Example:
                // Request: Match any(OR)/all(AND) of John or Smith on a comma-separated list column authors.data.
                // SQL: (authors.data LIKE (John) OR authors.data LIKE (John,%)
                //          OR authors.data LIKE (%, John) OR authors.data LIKE (%, John,%))
                //      OR/AND (authors.data LIKE (Smith) OR authors.data LIKE (Smith,%)
                //          OR authors.data LIKE (%, Smith) OR authors.data LIKE (%, Smith,%))
                $likes = array();
                $params = array();
                foreach ($values as $value) {
                    $innerlikes = array();
                    foreach ($sqlfields as $sqlfield) {
                        // List possible cases (first, in the middle, last, or only value).
                        $innerlikes[] = "(" . substr(str_repeat(" OR " . $DB->sql_like($sqlfield, '?', false, false), 4), 4) . ")";
                        $escapedvalue = $DB->sql_like_escape($value);
                        $params[] = "%, $escapedvalue,%";
                        $params[] = "$escapedvalue,%";
                        $params[] = "%, $escapedvalue";
                        $params[] = $escapedvalue;
                    }
                    $likes[] = "(" . implode(" OR ", $innerlikes) . ")";
                }
                $andor = $collatingtype == self::AND ? "AND" : "OR";
                return array(implode(" $andor ", $likes), $params);
            } else {
                // Request is: precisely match the text value with any of the request values.
                // Example:
                // Request: Match any of L1, L2 or M1 on a single text column academiclevel.data.
                // SQL: academiclevel.data IN (L1,L2,M1)
                // If on several fields, it would be for example:
                // course.name IN (Graphs,Algorithmics) OR course.fullname IN (Graphs,Algorithmics)
                list($insql, $inparams) = $DB->get_in_or_equal($values, SQL_PARAMS_QM);
                $ins = array();
                $params = array();
                foreach ($sqlfields as $sqlfield) {
                    $ins[] = "$sqlfield $insql";
                    $params = array_merge($params, $inparams);
                }
                return array("(" . implode(" OR ", $ins) . ")", $params);
            }

        } else {
            // Request is: search for one or more values contained in a field.
            // Example:
            // Request: Contain any(OR)/all(AND) of John or Smith on a column authors.data.
            // SQL: authors.data LIKE (%John%) OR/AND authors.data LIKE (%Smith%)
            // If on several fields, it would be for example:
            // (course.name LIKE (%Graphs%) OR course.fullname LIKE (%Graphs%))
            //  OR/AND (course.name LIKE (%Algorithmics%) OR course.fullname LIKE (%Algorithmics%))
            $likes = array();
            $params = array();
            foreach ($values as $value) {
                $innerlikes = array();
                foreach ($sqlfields as $sqlfield) {
                    $innerlikes[] = $DB->sql_like($sqlfield, '?', false, false);
                    $params[] = '%' . $DB->sql_like_escape($value) . '%';
                }
                $likes[] = "(" . implode(" OR ", $innerlikes) . ")";
            }
            $andor = $collatingtype == self::AND ? "AND" : "OR";
            return array(implode(" $andor ", $likes), $params);
        }
    }

}
